#ifndef _MSGDECODER_H_
#define _MSGDECODER_H_

#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>
#endif
#include "package.h"
#include "spmsgdecoder.hpp"
#include "spbuffer.hpp"
#include "sputils.hpp"

class MSGEXPRESS_API MsgDecoder : public SP_MsgDecoder {
public:
	MsgDecoder();
	~MsgDecoder();

	virtual int decode( SP_Buffer * inBuffer );

	SP_BlockingQueue * getQueue();

private:
	SP_BlockingQueue * mQueue;
};

#endif