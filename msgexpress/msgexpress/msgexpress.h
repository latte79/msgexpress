#ifndef _MSGEXPRESS_H_
#define _MSGEXPRESS_H_

namespace MsgExpress
{
	const static int Cmd_RegService = 0x00000001;
	const static int Cmd_Subscribe =  0x00000002;
	const static int Cmd_UnSubscribe= 0x00000003;
	const static int Cmd_ComplexSubscribe= 0x00000004;
};

#endif