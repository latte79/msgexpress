
#ifndef __sslog_DATETIME_H
#define __sslog_DATETIME_H 1

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <string>


typedef long long int64_t;
typedef unsigned long long uint64_t;


namespace sslog{

class date{
private:
	time_t time_ ;
	int year_ ;		//年2011
	int month_ ;	//月1...12
	int day_ ;		//日期1...31
	int wday_ ;		//周1...7

	std::string datestr_ ;

	/*
		2011-11-23
		返回当地时间0点的秒数，输入参数为gmt秒数，由time(NULL)返回 ;
	*/
	time_t calc_date(time_t time , int& year , int& month , int& day , int& wday) ;

	int calc_stage_ ;
public:
	date() ;
	date(time_t ts) ;
	date(int year , int month , int day) ;
	void reset() ;

	date& operator=(time_t ts) ;
	bool operator==(time_t ts) ;
	bool operator!=(time_t ts) ;

	bool operator==(const date& d) ;
	bool operator!=(const date& d) ;

	bool operator<(const date& d) ;
	bool operator<=(const date& d) ;
	bool operator>(const date& d) ;
	bool operator>=(const date& d) ;

	std::string& to_string() ;
	time_t time() ;
	time_t gmttime() ;
	bool calc_now() ;

	inline int year() const {return year_ ;}
	inline int month() const {return month_ ;}
	inline int day() const {return day_ ;}
	inline int wday() const {return wday_ ;}

	date& next_day(int dc = 1) ;

	static date& today() ;
	static int timezone() ;
} ;

class utime{
protected:
	int hour_ ;
	int minute_ ;
	int second_ ;
	int usec_ ;

	std::string usecstr_ ;

	void calcutestr() ;
	int calc_stage_ ;
public:
	utime(bool need_calc = false) ;//支持微秒
	~utime() ;

	void reset() ;
	/*
		2011-11-24
		micro必须是本地时间，乘以1,000,000。得到微秒
	*/
	void calc_now(int64_t micro = 0) ;

	inline int hour() const {return hour_ ;}
	inline int minute() const {return minute_ ;}
	inline int second() const {return second_ ;}
	inline int usec() const {return usec_ ;}

	std::string& to_string() ;

	static int64_t now() ;
	const utime& operator=(const utime& src) ;
} ;

}

#endif /** __MSGBUS_DATETIME_H */
