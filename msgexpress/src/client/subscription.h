#pragma once

#include <map>
#include "../inc/protobuf.h"
#include "subscription_inft.h"

class Subscription : public ISubscription
{
private:
	struct OperValue
	{
		MsgExpress::Operator oper;
		std::string value;
	};
	class ValueData
	{
	public:
		MsgExpress::DataType type;
		std::vector<OperValue*> vecValues;
		~ValueData()
		{
			std::vector<OperValue*>::iterator it=vecValues.begin();
			while(it!=vecValues.end())
			{
				delete *it;
				it++;
			}
			vecValues.clear();
		}
	};

	int mId;
	int mTopic;
	unsigned int mAddr;
	std::vector<int> mIndex;
	std::map<int,ValueData*> mapKeyValues;
private:
	bool AddValue(int key,MsgExpress::DataType type,std::string value,MsgExpress::Operator oper=MsgExpress::Equal);
	Subscription();
public:
	Subscription(int id,int topic,unsigned int addr);
	virtual ~Subscription();
    
public:
	virtual void AddIndex(int index);
	virtual bool AddString(int key,std::string value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddRaw(int key,const char * str,size_t size,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddInt32(int key,int value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddUInt32(int key,unsigned int value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddInt64(int key,int64_t value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddUInt64(int key,uint64_t value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddFloat(int key,float value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddDouble(int key,double value,MsgExpress::Operator oper=MsgExpress::Equal);
	virtual bool AddDatetime(int key,time_t value,MsgExpress::Operator oper=MsgExpress::Equal);

	virtual void GetSubData(MsgExpress::SubscribeData& sub);
};
