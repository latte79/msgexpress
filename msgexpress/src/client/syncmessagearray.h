#pragma once
#include <mutex>
#include <condition_variable>

#include "../inc/package.h"


#ifdef  __linux__
#define TRUE                1
#define FALSE               0
#define WAIT_TIMEOUT        258L
typedef unsigned long       DWORD;
#endif

#define MaxSyncSerialNum  0xFFFF

class SyncMessageArray
{
public:
	///@brief 同步结构
	struct SyncMessageItem
	{
		PackagePtr response;
		bool used;
		///@brief 消息的同步事件
		std::mutex mutex;
		std::condition_variable  cond;
		SyncMessageItem()
		{
			used = false;
		}
	};
public:
	SyncMessageArray();
	~SyncMessageArray(void);
	void Clear();
	///@brief 注册一个等待应答的同步消息
	bool RegistSyncMsg(int SerialNum);
	///@brief 反注册一个同步消息,
	void UnRegistSyncMsg(int SerialNum);
	///@brief 将服务端返回的同步消息应答复制到缓冲区并发信号通知等待应答的线程.
	bool CopyMessageAndSendSignal(int SerialNum, PackagePtr ResponsePkg);
	///@brief 等待应答,等待时间TimeOut毫秒,否则超时
	DWORD WaitForResponse(int SerialNum, PackagePtr& ResponsePkg, DWORD TimeOut);
private:
	std::mutex main_mutex;
	SyncMessageItem sync_msg_arr[MaxSyncSerialNum];
	int msg_count;
};
