#include "syncmessagearray.h"
#include "../inc/logger.h"
#include <thread>

SyncMessageArray::SyncMessageArray(void)
{
	msg_count = 0;
}

SyncMessageArray::~SyncMessageArray(void)
{
}
void SyncMessageArray::Clear()
{
	LOG4_INFO("nCount = %d", msg_count);
	int nTemp = 0;
	while (msg_count > 0)
	{
		nTemp++;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		if (nTemp > 200)
		{
			LOG4_ERROR("Wait UnRegistSyncMsg timeout!");
			break;
		}
	}
	LOG4_INFO("nCount = %d", msg_count);
}
///@brief 注册一个等待应答的同步消息
bool SyncMessageArray::RegistSyncMsg(int SerialNum)
{
	std::unique_lock<mutex> lock(main_mutex);
	if (SerialNum >= MaxSyncSerialNum || sync_msg_arr[SerialNum].used)
	{
		LOG4_ERROR("Serial Num is used,NO=%d",SerialNum);
		return false;
	}
	sync_msg_arr[SerialNum].used = TRUE;
	msg_count++;
	return true;
}
///@brief 反注册一个同步消息,
void SyncMessageArray::UnRegistSyncMsg(int SerialNum)
{
	if (SerialNum >= MaxSyncSerialNum)
	{
		return ;
	}
	if (!sync_msg_arr[SerialNum].used)
	{
		LOG4_ERROR("Serial Num is not registered,NO=%d",SerialNum);
		return;
	}
	//LOG4_INFO("UnRegister Serial Num,NO=%d",SerialNum);
	std::unique_lock<mutex> lock(main_mutex);
	sync_msg_arr[SerialNum].used = false;
	sync_msg_arr[SerialNum].response.reset();
	msg_count--;
}
///@brief 将服务端返回的同步消息应答复制到缓冲区并发信号通知等待应答的线程.
bool SyncMessageArray::CopyMessageAndSendSignal(int SerialNum, PackagePtr ResponsePkg)
{
	std::unique_lock<mutex> lock(main_mutex);
	if (SerialNum >= MaxSyncSerialNum || !sync_msg_arr[SerialNum].used)
		return false;
	std::unique_lock<mutex> lk(sync_msg_arr[SerialNum].mutex);
	sync_msg_arr[SerialNum].response = ResponsePkg;//复制应答消息
	sync_msg_arr[SerialNum].cond.notify_all();
	return true;
}
DWORD SyncMessageArray::WaitForResponse(int SerialNum, PackagePtr& ResponsePkg, DWORD TimeOut)
{
	if (SerialNum >= MaxSyncSerialNum || !sync_msg_arr[SerialNum].used)
	{
		LOG4_ERROR("Serial Num is not in use,NO=%d",SerialNum);
		return -1;
	}
	std::unique_lock<mutex> lock(sync_msg_arr[SerialNum].mutex);
	if (sync_msg_arr[SerialNum].cond.wait_for(lock, std::chrono::milliseconds(TimeOut)) == std::cv_status::timeout)
	{
		UnRegistSyncMsg(SerialNum);
		return -1;
	}
	lock.unlock();
	ResponsePkg = sync_msg_arr[SerialNum].response;
	UnRegistSyncMsg(SerialNum);
	return 0;
}
