#pragma once

#include "../inc/msgexpress.h"

MSGEXPRESS_API int startdesktopserver();
MSGEXPRESS_API int startbroker();
MSGEXPRESS_API int startgateway();
MSGEXPRESS_API int startgwproxy();
MSGEXPRESS_API int startproxy();
