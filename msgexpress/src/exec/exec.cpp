// broker.cpp : 定义控制台应用程序的入口点。
//

#include "../core/servicemanager.h"
#include "../inc/logger.h"
#include "../core/broker_config.h"
#include "../gateway/gateway.h"
#include "exec.h"
#include "../server/broker.h"


//---------------------------------------------------------
using namespace google::protobuf;

#ifdef _WIN32
int startdesktopserver()
{
	//TCHAR szTemp[MAX_PATH] = {0};
	//TCHAR szServerPath[MAX_PATH] = {0};
 //   GetModuleFileName(NULL, szTemp, MAX_PATH);
	//::GetLongPathName(szTemp,szServerPath,  MAX_PATH);

 //   wstring log_file = L"qserver.properties";
	//LogInit(log_file.c_str());
	//
	//if(!CMainCacheApp::GetInstance().Init(szServerPath))
	//{
	//	LOG4_ERROR("Create share memory failed.");
	//	return -1;
	//}

	//if (!SetQServerExist())
	//{
	//	LOG4_INFO("QServer is running,exit current process!");
	//	return 0;
	//}
	//ServerConfig sconf;
	//if(!sconf.LoadFile("brokercfg.xml"))
	//{   
	//	LOG4_ERROR("read server xml config file error");
	//	return -1;
	//}   
	//LOG4_INFO("QServer init,port=%d,version=%s",sconf.port_,LIBVERSION);

	//int port = sconf.port_, maxThreads = sconf.thread_;

	//if( 0 != sp_initsock() ) assert( 0 );

	//ServiceManager manager("brokercfg.xml");

	//SP_IocpLFServer server( "127.0.0.1", port, new MsgHandlerFactory( &manager ) );

	//server.setTimeout( sconf.connect_time_out_ );
	//server.setMaxThreads( maxThreads );
	//server.setMaxConnections(sconf.max_client_);
	//server.setReqQueueSize( 100, "Sorry, server is busy now!\r" );

	//if(server.run()!=0)
	//{
	//	LOG4_ERROR("Start QServer failed");
	//	return -1;
	//}
	//else
	//{
	//	SetQServerOK();
	//	
	//	HANDLE handle=GetCurrentProcess();
	//    DWORD processId=GetProcessId(handle);
	//    //记录processid
	//    SetMainProcess(processId);

	//	DesktopGatewayProxy proxy;
	//    proxy.StartB("desktopgatewayproxy.xml");
	//	int count=0;
	//	while(1)
	//	    Sleep(1000);
	//}
	//server.shutdown();
	//
	//sp_closelog();
	return 0;
}
#endif
void InitLog()
{
#ifdef _WIN32
	wstring log_file = L"log4cxx.properties";
	LogInit(log_file.c_str());
#elif __linux__
	string log_file = "log4cxx.properties";
	LogInit(log_file.c_str());
#endif
}
int startserver(const string& cfgfile)
{
	InitLog();
	ServerConfig sconf;
	if (!sconf.LoadFile(cfgfile))
	{
		LOG4_ERROR("read server xml config file error");
		return 0;
	}

	SetLevel(sconf.loglevel);
	LOG4_INFO("Server(version=%s) start,listen on-%s:%d", LIBVERSION, sconf.host_.c_str(), sconf.port_);

	int port = sconf.port_, maxThreads = sconf.thread_;

	ServiceManager manager(cfgfile);
	BrokerServer broker(&manager, cfgfile);
	broker.start();
	
	MsgExpressProxy proxy;
	if (sconf.clustermode!=enumClusterMode::None && sconf.clusterCfg.size() > 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		LOG4_INFO("Proxy Start!,libVersion=%s", LIBVERSION);
		proxy.Start(sconf.clusterCfg.c_str());
	}

	while (1)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(30000));
	}
	LOG4_ERROR("Broker over!");
	return 0;
}
int startbroker()
{
    return startserver("brokercfg.xml");
}
int startgateway()
{
	//start gwproxy
	InitLog();
	LOG4_INFO("GWproxy Start! libVersion=%s",LIBVERSION);
	GatewayProxy proxy;
	proxy.Start("gatewayproxy.xml");

    return startserver("gatewaycfg.xml");
}

int startgwproxy()
{
    InitLog();
	LOG4_INFO("GWproxy Start! libVersion=%s",LIBVERSION);
	GatewayProxy proxy;
	proxy.Start("gatewayproxy.xml");
	while(1)
	{
		proxy.Print();
		std::this_thread::sleep_for(std::chrono::milliseconds(30000));
	}
	return 0;
}

int startproxy()
{
    InitLog();
	LOG4_INFO("Proxy Start!,libVersion=%s",LIBVERSION);
	MsgExpressProxy proxy;
	proxy.Start("proxy.xml");
	while(1)
	{
		proxy.Print();
		std::this_thread::sleep_for(std::chrono::milliseconds(30000));
	}
	return 0;
}
