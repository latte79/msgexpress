#ifndef _PGMSENDER_H_
#define _PGMSENDER_H_

#include <cassert>
#include <clocale>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#ifndef _WIN32
#	include <unistd.h>
#	include <netinet/in.h>
#	include <sys/socket.h>
#else
#	include "getopt.h"
#endif
#ifdef __APPLE__
#	include <pgm/in.h>
#endif
#include <pgm/pgm.h>

static bool is_terminated = false;

#ifndef _WIN32
static int terminate_pipe[2];
static void on_signal(int);
#else
static WSAEVENT terminateEvent;
static bool on_console_ctrl(DWORD);
#endif

class PgmRecv {
public:
	PgmRecv();
	~PgmRecv();
    bool init(const char *address, unsigned int port);
public:
	bool pgm_recv(char* buffer,size_t &len);
	int on_data(const void*, size_t, const ip::pgm::endpoint&);

private:
	unsigned int port;
	std::string network;
	bool use_multicast_loop;
	int	udp_encap_port;
	
	int max_tpdu;
	int sqns;
	
	bool use_fec;
	int rs_k;
	int rs_n;
	
	cpgm::pgm_error_t* pgm_err;
	ip::pgm::endpoint *endpoint;
	ip::pgm::socket *sock;
private:
	bool on_startup(void);
};

#endif
