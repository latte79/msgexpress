#ifndef _PGMSEND_H_
#define _PGMSEND_H_

#include <clocale>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#ifndef _WIN32
#	include <unistd.h>
#	include <netinet/in.h>
#else
#	include "getopt.h"
#endif
#ifdef __APPLE__
#	include <pgm/in.h>
#endif
#include <pgm/pgm.h>

class PgmSend {
public:
	PgmSend();
	~PgmSend();
    bool init(const char *address, unsigned int port, unsigned int rate_times = 1);
public:
	bool pgm_send(const char *buf,unsigned int size);

private:
	int port;
	std::string network;
	bool use_multicast_loop;
	int udp_encap_port;

	int max_tpdu;
	int max_rte;
	int sqns;

	bool use_fec;
	int rs_k;
	int rs_n;

	ip::pgm::endpoint *endpoint;
	ip::pgm::socket *sock;

private:
	bool create_sock(void);
};

#endif
