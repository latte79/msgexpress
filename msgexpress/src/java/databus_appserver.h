#pragma once

#include "jni.h"

#ifdef __cplusplus
extern "C" {
#endif

	/*
 * Class:     databus_AppServer
 * Method:    CreateServer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_databus_AppServer_CreateServer
  (JNIEnv *, jobject);

/*
 * Class:     databus_AppServer
 * Method:    Initialize
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_databus_AppServer_Initialize
  (JNIEnv *, jobject,jint,jstring,jstring);
/*
 * Class:     databus_AppServer
 * Method:    Release
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_Release
  (JNIEnv *, jobject,jint);

/*
 * Class:     databus_AppServer
 * Method:    PostMsg
 * Signature: (Ldatabus/Package;)Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_PostMsg
  (JNIEnv *, jobject,jint, jobject);

/*
 * Class:     databus_AppServer
 * Method:    SendMsg
 * Signature: (Ldatabus/Package;Ldatabus/Package;I)Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_SendMsg
  (JNIEnv *, jobject,jint, jobject, jobject, jint);

/*
 * Class:     databus_AppServer
 * Method:    QueryData
 * Signature: (Ldatabus/Package;Ldatabus/Package)Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_QueryData
  (JNIEnv *, jobject,jint, jobject, jobject);

/*
 * Class:     databus_AppServer
 * Method:    Reply
 * Signature: (Ldatabus/PackageHeader;[B)Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_Reply
  (JNIEnv *, jobject,jint, jobject, jbyteArray);

/*
 * Class:     databus_AppServer
 * Method:    GetPackage
 * Signature: (Ldatabus/Package;)Z
 */
JNIEXPORT jboolean JNICALL Java_databus_AppServer_GetPackage
  (JNIEnv *, jobject,jint, jobject);

/*
 * Class:     databus_AppServer
 * Method:    GetClazz
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_databus_AppServer_GetClazz
  (JNIEnv *, jobject, jint, jint);

/*
 * Class:     databus_AppServer
 * Method:    GetCommand
 * Signature: (Ljava/lang/String;II)I
 */
JNIEXPORT jint JNICALL Java_databus_AppServer_GetCommand
  (JNIEnv *, jobject, jstring, jint, jint);

/*
 * Class:     databus_AppServer
 * Method:    GetLastError
 * Signature: (Ldatabus/ErrMessage;)V
 */
JNIEXPORT void JNICALL Java_databus_AppServer_GetLastError
  (JNIEnv *, jobject,jint, jobject);

#ifdef __cplusplus
}
#endif

