
#include <functional>
#include "proxy.h"
#include "../inc/logger.h"
#include "../core/command_config.h"
#include "proxyclient_config.h"

ProxyClient::ProxyClient()
{
}

ProxyClient::~ProxyClient()
{
}

void ProxyClient::OnRawPackage(PackagePtr package)
{
	if(funOnRawPackage)
		funOnRawPackage(package);
}

void ProxyClient::OnEvent(int eventId)
{
	if(funOnEvent)
		funOnEvent(eventId);
}

MsgExpressProxy::MsgExpressProxy()
{
	m_pServerA=new ProxyClient();
	m_pServerA->funOnEvent=bind(&MsgExpressProxy::OnEventA,this,_1);
	m_pServerA->onRawData=bind(&MsgExpressProxy::OnRawDataA,this,_1,_2);
	m_pServerA->funOnRawPackage=bind(&MsgExpressProxy::OnRawPackageA,this,_1);

	m_pServerB=new ProxyClient();
	m_pServerB->funOnEvent=bind(&MsgExpressProxy::OnEventB,this,_1);
	m_pServerB->funOnRawPackage=bind(&MsgExpressProxy::OnRawPackageB,this,_1);

	serialnum = 0;

#ifdef _WIN32
	mapInfoA.rehash(0x10000);
	float factor=mapInfoA.max_load_factor();
	mapInfoB.rehash(0x10000);
#elif __linux__
    mapInfoA.resize(0x10000);
    mapInfoB.resize(0x10000);
#endif
}

MsgExpressProxy::~MsgExpressProxy()
{
	m_pServerA->Release();
    m_pServerB->Release();
	delete m_pServerA;
	delete m_pServerB;
	m_pServerA = NULL;
    m_pServerB = NULL;
}
void MsgExpressProxy::Init(const ProxyClientConfig &pclientconf)
{
}
bool MsgExpressProxy::Start(const char* configFile)
{
	string xml_file(configFile);
	ProxyClientConfig &pclientconf=ProxyClientConfig::Instance();
	if(!pclientconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read proxy client xml config file error");
		return false;
	} 
	Init(pclientconf);
	ClientCFG cfg = pclientconf.clientCfg;
	if(m_pServerA->Initialize(&cfg,NULL)!=0)
	{
		LOG4_ERROR("A Connect to Data bus[%s:%d] failed.",cfg.serverGroup[0].serverIP.c_str(),cfg.serverGroup[0].port);
		//return false;
	}
	cfg = pclientconf.clientCfgB;
	if(m_pServerB->Initialize(&cfg,NULL)!=0)
	{
		LOG4_ERROR("B Connect to Data bus[%s:%d] failed.",cfg.serverGroup[0].serverIP.c_str(),cfg.serverGroup[0].port);
		//m_pServerA->Release();
		//return false;
	}

	LOG4_INFO("Proxy server start OK!");
	return true;
}
void MsgExpressProxy::Print()
{
	LOG4_INFO("Proxy status OK!");
}
void MsgExpressProxy::Exchange(PackagePtr package, AppServerImpl* pServer, hash_map<unsigned int, ItemInfoPtr>& mapA, hash_map<unsigned int, ItemInfoPtr>& mapB, std::mutex& mutexA, std::mutex& mutexB)
{
	if(package->IsRequest())
	{
		unsigned int SerialNum = ++serialnum;
		unsigned int addr=pServer->GetAddress();
		std::unique_lock<std::mutex> lock(mutexA);
		ItemInfoPtr info=mapA[SerialNum];
		if(info==NULL){
			info=ItemInfoPtr(new ItemInfo());
			mapA[SerialNum]=info;
		}
		else{
			LOG4_ERROR("Proxy server has no replied message,serialNum=%d,srcAddr=%d",info->serialNum,info->srcAddr);
		}
		info->sendtime = gettimepoint();
		lock.unlock();
		info->serialNum=package->GetSerialNum();
		info->srcAddr=package->GetSrcAddr();
		package->SetSerialNum(SerialNum);
		package->SetSrcAddr(addr);
		SendDataItem* item=new SendDataItem(package->content(),package->packagesize());
		if(!((AppServerImpl*)pServer)->postRawData(item))
		{
			delete item;
			LOG4_ERROR("Proxy post msg failed,msg:%s",package->DebugString().c_str());
		}
	}
	else if(package->IsResponse())
	{
		unsigned int SerialNum = package->GetSerialNum();
		std::unique_lock<std::mutex> lock(mutexB);
		hash_map<unsigned int,ItemInfoPtr>::iterator it=mapB.find(SerialNum);
		if(it==mapB.end()){
			LOG4_ERROR("Response to proxy can't find map information,Msg:%s",package->DebugString().c_str());
			return;
		}
		ItemInfoPtr info=it->second;
		if(!package->header().ismultipage() || package->header().pageno()==0)
		    mapB.erase(it);
		lock.unlock();
		uint64_t now = gettimepoint();
		int off=now-info->sendtime;
		if(off>100)
			LOG4_WARN("Cost too many time(%d ms),src serial:%d,content:%s",off,info->serialNum,package->DebugString().c_str());

		package->SetSerialNum(info->serialNum);
		package->SetDstAddr(info->srcAddr);
		SendDataItem* item=new SendDataItem(package->content(),package->packagesize());
		if(!((AppServerImpl*)pServer)->postRawData(item))
		{
			delete item;
			LOG4_ERROR("Proxy post msg failed,msg:%s",package->DebugString().c_str());
		}
	}
	else if(package->IsPublish())
	{
		auto func = [] (const MsgParams& param) { 
			if(param.result!=0)
			{
				LOG4_ERROR("Publish failed:%s,mag:%s",param.errmsg.c_str(),param.req->DebugString().c_str());
			}
		};
		if (!package->AddBrokerTrace(((AppAddress)pServer->GetAddress()).Broker.Id))
		{
			//防止推送消息来回震荡
			LOG4_ERROR("Publish message loop on proxy,msg:%s", package->DebugString().c_str());
			return;
		}
		AppAddress srcAddr = package->GetSrcAddr();
		AppAddress dest=(AppAddress)pServer->GetAddress();
		LOG4_INFO("src broker:%d,dest broker:%d,serial:%d", (int)srcAddr.Broker.Id, (int)dest.Broker.Id, package->GetSerialNum());
		/*AppAddress srcAddr = package->GetSrcAddr();
		if (((AppServerImpl*)pServer)->GetConfig().multipageswitch)//暂时用这个屏蔽新版本不兼容老版本的问题
		{
			if (srcAddr.Broker == ((AppAddress)pServer->GetAddress()).Broker)
			{
				//防止推送消息来回震荡
				LOG4_ERROR("Publish message loop on proxy,msg:%s", package->DebugString().c_str());
				return;
			}
		}*/
		package->SetSerialNum(0);
		SendDataItem* item=new SendDataItem(package->content(),package->packagesize());
		if(!((AppServerImpl*)pServer)->postRawData(item))
		{
			delete item;
			LOG4_ERROR("Proxy post msg failed,msg:%s",package->DebugString().c_str());
		}
	}
}

void MsgExpressProxy::OnRawDataA(const char* buf,unsigned int size)
{
	m_pServerB->postRawData(buf,size);
}

void MsgExpressProxy::OnRawPackageA(PackagePtr package)
{
	Exchange(package,m_pServerB,mapInfoA,mapInfoB,mMutexA,mMutexB);
}

void MsgExpressProxy::OnRawPackageB(PackagePtr package)
{
	Exchange(package,m_pServerA,mapInfoB,mapInfoA,mMutexB,mMutexA);
}
void MsgExpressProxy::OnEventA(int eventId)
{
	if(eventId==1)
	{
	    LOG4_INFO("Proxy:status changed on side A,status:%d",eventId);
	}
	else
		LOG4_ERROR("Proxy:status changed on side A,status:%d",eventId);
}
void MsgExpressProxy::OnEventB(int eventId)
{
	if(eventId==1)
	{
	    LOG4_INFO("Proxy:status changed on side B,status:%d",eventId);
	}
	else
		LOG4_ERROR("Proxy:status changed on side B,status:%d",eventId);
}