#pragma once

#include <string>
#include "../inc/service_config.h"
#include "../client/client_config.h"

using namespace std;

class  ProxyClientConfig:public ClientConfig
{
private:
    ProxyClientConfig(){};
public:
    ~ProxyClientConfig(){};

    static ProxyClientConfig& Instance()
    {
        static ProxyClientConfig conf;
        return conf;
    }
    virtual bool ReadConfigFile();
private:
    bool ReadProxyClientCFG(TiXmlElement *root);
public:
	ClientCFG clientCfgB;
    string storage_ip;
	int storage_port;
	int storage_db;
};
