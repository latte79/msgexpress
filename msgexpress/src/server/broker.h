#pragma once
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>
#include <event2/thread.h>
#include <event.h>

#include "session_manager.h"
#include "../core/servicemanager.h"
#include "../core/threadpool.h"
#include "../core/broker_config.h"
#include "../core/utils.h"
#include "../inc/clientbase.h"
using namespace std;

struct ThreadArg
{
	void *arg;
	unsigned int index;
};



class BrokerServer
{
private:
	bool isMaster;
	time_t startTime;
	int brokerId;
	int brokerHeartbeat;
	ServerConfig sconf;
	struct event_base * base ;
	// libevent的高级API专为监听的FD使用 
	struct evconnlistener * listener ;
	// 信号处理event指针 
	struct event * signal_event ;
	struct event * timeout_event;
	SessionManager sessionManager;
	ServiceManager * mServiceManager;
	DataBusClient  client;
	int mIsShutdown;
	int mMaxThreads;
	threadpool executor;
	threadpool sendThread;
	std::vector<std::thread> sequencePool;
	//std::vector<std::thread> unsequencePool;
	SafeQueue * mSequenceTaskQueue;
	SafeQueue  mUnsequenceTaskQueue;
	thread configThread;
	
	std::mutex mutexThread;
	std::unordered_map<string, string> mapThread;
private:
	int acceptLoop(void* arg);

	void sequenceTaskLoop(unsigned int index);
	void unsequenceTaskLoop(unsigned int index);
	void handleOneTask(Task* task);
	void handleOneEvent();
	void handleOneComplete();
	
	void publishBrokerInfo();
	void publishAppServer( int serviceId);
	void noticeServer();
	void kickSession(int type);
	void kickAll();

	void setThreadName();
public:
	void pushTask(PackagePtr package, AppAddress src);
	void dispatch(const vector<shared_ptr<Response>>& vecMsg);
	void addSendTask(function<void(void*)> fun,void*arg);
	void onAccept(evutil_socket_t fd, struct sockaddr * sa);
	void onTimer();
public:
	BrokerServer(ServiceManager*,const string& cfgfile);
	~BrokerServer();
	int start();
	ServerConfig& getConfig(){ return sconf; }
	AppAddress getAddress();
	bool isMasterBroker(){ return isMaster; }
	bool isSlaveBroker(){ return !isMaster; }
	time_t getStartTime(){ return startTime; }
	void updateBrokerInfo(MsgExpress::BrokerInfo* info);
	void updateService(int serviceId);
public:
	
};

