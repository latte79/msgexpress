
#pragma once
#ifdef _WIN32

#include <shlwapi.h>

typedef struct tagCacheData
{
    DWORD dwMainProcessId;
    int StartupCount;
    int CrashCount;
    TCHAR BinDir[1024];
    TCHAR ServerPath[1024];
    TCHAR ClientDir[1024];
    TCHAR RootDir[1024];
    char BoxOpenMutexName[32];
    char BoxOkMutexName[32];

} MainCacheData;

class CMainCacheApp
{
public:
	CMainCacheApp();
	~CMainCacheApp();
	bool Init(wchar_t* serverpath);
	MainCacheData* GetCacheData();

	static CMainCacheApp& GetInstance()
	{
		static CMainCacheApp theApp;
		return theApp;
	}
private:
    MainCacheData* mpCacheData;
    HANDLE mhMapFile;
};

void  GenerateMutexName();
bool  IsQServerExist(bool showinfo=true);
bool  SetQServerExist();
bool  IsQServerOK(bool showinfo=true);
bool  SetQServerOK();
bool  StartupQServer();
void  ClearHandle();
unsigned long  KillQServer();

///@brief 设置main进程句柄
void  SetMainProcess(unsigned long dwProcessID);
///@brief 获取main进程句柄
unsigned long  GetMainProcess();
///@brief 记录主程序被重启次数
void  AddStartupCount();
///@brief 获取主程序被重启次数
int  GetStartupCount();
///@brief 清零主程序被重启次数
void  ClearStartupCount();
///@brief 程序崩溃后调用，记录崩溃次数
void  AddCrashCount();
///@brief 程序崩溃后准备重启时调用，看看崩溃了多少次
int  GetCrashCount();
///@brief 设置当前可执行程序路径
void  SetBinDir(TCHAR* Dir);
///@brief 获得当前可执行程序路径
const TCHAR*  GetBinDir();
///@brief 获得当前可执行程序路径
const TCHAR*  GetRootDir();
///@brief 设置当前可执行程序路径
void  SetServerPath(TCHAR* Path);
///@brief 获得当前可执行程序路径
const TCHAR*  GetServerPath();

#endif //define win32