

#include "desktop.h"

#ifdef _WIN32

#include <tchar.h>
#include <string.h>
#include <time.h>
#include "../inc/logger.h"

#pragma comment( lib, "shlwapi.lib") 

const int MAX_CONNECTION = 100;
const int MAXREGISTER=10;
const int MAXPACKAGESIZE=256;


#pragma warning(disable : 4996) 



CMainCacheApp::CMainCacheApp()
    :mpCacheData(NULL),
    mhMapFile(NULL)
{
    
}
bool CMainCacheApp::Init(wchar_t* serverpath)
{
	TCHAR szName[MAX_PATH] = {60}, szName1[MAX_PATH] = {0};
    TCHAR* p = NULL;
	TCHAR szTemp[MAX_PATH] = {0};
    GetModuleFileName(NULL, szTemp, MAX_PATH);
	::GetLongPathName(serverpath, szName, MAX_PATH);		// szTemp 可能是缩写路径，需要转换为全路径
    PathRemoveFileSpec(szName);
    PathCanonicalize(szName1, szName);
    while (p = _tcschr(szName1, _T('\\')))
        *p = _T(':');

	// 将路径转化为小写字符，防止由于大小写不同导致创建不同的共享内存对象
	_tcslwr(szName1); 
        
    mhMapFile = OpenFileMapping(
                    FILE_MAP_ALL_ACCESS,   // read/write access
                    FALSE,                 // do not inherit the name
                    szName1);               // name of mapping object 

    bool bNew = (mhMapFile == NULL);
    if (bNew)                           // not created yet
    {
        mhMapFile = CreateFileMapping(
                        INVALID_HANDLE_VALUE,    // use paging file
                        NULL,                    // default security 
                        PAGE_READWRITE,          // read/write access
                        0,                       // max. object size 
                        sizeof(MainCacheData),   // buffer size  
                        szName1);                 // name of mapping object
    }
    if (!mhMapFile)
    {
        DWORD err = GetLastError();
        return false;
    }

    mpCacheData = (MainCacheData*) MapViewOfFile(mhMapFile,   // handle to map object
                        FILE_MAP_ALL_ACCESS, // read/write permission
                        0,                   
                        0,                   
                        sizeof(MainCacheData));    
    if (bNew)
    {
        SecureZeroMemory(mpCacheData, sizeof(MainCacheData));
        //InitializeCriticalSection(&mpCacheData->oConnectionSection);
    }
	SetServerPath(serverpath);
	return true;
}
CMainCacheApp::~CMainCacheApp()
{
    if (mhMapFile)
    {
        UnmapViewOfFile(mpCacheData);
        CloseHandle(mhMapFile);
        mhMapFile = NULL;
        mpCacheData = NULL;
    }
}

MainCacheData* CMainCacheApp::GetCacheData()
{
    return mhMapFile ? mpCacheData : NULL;
}

MainCacheData* GetCacheData()
{
	return CMainCacheApp::GetInstance().GetCacheData();
}


void  GenerateMutexName()
{
	if(strlen(GetCacheData()->BoxOpenMutexName)>0)
		return;
	time_t t;
	time(&t);
	srand((int)t);
	for(int i=0;i<31;i++)
		GetCacheData()->BoxOpenMutexName[i]= rand() % 26 +'a';
	GetCacheData()->BoxOpenMutexName[0]='a';
	GetCacheData()->BoxOpenMutexName[31]= 0;
	for(int i=0;i<31;i++)
		GetCacheData()->BoxOkMutexName[i]= rand() % 26 +'a';
	GetCacheData()->BoxOkMutexName[0]='a';
	GetCacheData()->BoxOkMutexName[31]= 0;
	LOG4_INFO("GenerateMutexName,QServer Open=%s,QServer OK=%s",GetCacheData()->BoxOpenMutexName,GetCacheData()->BoxOkMutexName);
}
bool  IsQServerExist(bool showinfo)
{
	if(strlen(GetCacheData()->BoxOpenMutexName)==0)
	{
		if(showinfo)
		    LOG4_INFO("Is QServer Exist=FALSE,name is null");
		return FALSE;
	}
	HANDLE hMutex = OpenMutexA(MUTEX_ALL_ACCESS, false, GetCacheData()->BoxOpenMutexName);
	if(showinfo)
	    LOG4_INFO("Is QServer Exist ,handle=%d",hMutex);
	bool ret=( hMutex!=NULL);
	CloseHandle(hMutex);
	return ret;
}
bool  SetQServerExist()
{
	if(strlen(GetCacheData()->BoxOpenMutexName)==0)
		GenerateMutexName();
	HANDLE hMutex = CreateMutexA(NULL, false, GetCacheData()->BoxOpenMutexName);
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		LOG4_INFO("Set QServer Exist Failed");
		return FALSE;
	}
	LOG4_INFO("Set QServer Exist OK,handle=%d",hMutex);
	return TRUE;
}
bool  IsQServerOK(bool showinfo)
{
	if(strlen(GetCacheData()->BoxOkMutexName)==0)
	{
		if(showinfo)
		    LOG4_INFO("IsQServerOK=FALSE,name is null");
		return FALSE;
	}
	HANDLE hMutex = OpenMutexA(MUTEX_ALL_ACCESS, false,GetCacheData()->BoxOkMutexName);
	if(showinfo)
	    LOG4_INFO("IsQServerOK handle=%d",hMutex);
	bool ret=( hMutex!=NULL);
	CloseHandle(hMutex);
	return ret;
}
bool  SetQServerOK()
{
	if(strlen(GetCacheData()->BoxOkMutexName)==0)
	{
		LOG4_INFO("SetQServerOK failed,name is null");
		return FALSE;
	}
	HANDLE hMutex = CreateMutexA(NULL, false, GetCacheData()->BoxOkMutexName);
	LOG4_INFO("SetQServerOK handle=%d",hMutex);
	return hMutex!=NULL;
}

bool  StartupQServer()
{
	// 增加调用者输出
	char szTemp[MAX_PATH] = {0};
	char szCallerPath[MAX_PATH] = {0};
	::GetModuleFileNameA(NULL, szTemp, MAX_PATH);
	::GetLongPathNameA(szTemp, szCallerPath, MAX_PATH);		
	int nParentProcessID = ::GetCurrentProcessId();
	LOG4_INFO("StartupQServer Caller=%s, ProcessID=%d", szCallerPath, nParentProcessID);
	
	//新启动一个
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	TCHAR oldDir[1024], newDir[1024];
	GetCurrentDirectory(1024,oldDir);

    _tcscpy(newDir, GetServerPath());
	::PathRemoveFileSpec(newDir);
	SetCurrentDirectory(newDir);
	LOG4_INFO("CreateProcess");
	bool ret=(bool)CreateProcess(L"QServer.exe", L"", NULL, NULL, FALSE, DETACHED_PROCESS | CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	LOG4_INFO("CreateProcess OK");
	SetCurrentDirectory(oldDir);
	return ret;
}
void  ClearHandle()
{
	HANDLE hMutex = CreateMutexA(NULL, false, GetCacheData()->BoxOpenMutexName);
	if (hMutex)
	{
		ReleaseMutex(hMutex);
		CloseHandle(hMutex);
	}
	hMutex=0;
	hMutex = CreateMutexA(NULL, false, GetCacheData()->BoxOkMutexName);
	if (hMutex)
	{
		ReleaseMutex(hMutex);
		CloseHandle(hMutex);
	}
	GetCacheData()->BoxOpenMutexName[0]=0;
	GetCacheData()->BoxOkMutexName[0]=0;
	LOG4_INFO("ClearHandle");
}
DWORD  KillQServer()
{
	bool ret=FALSE;
	DWORD dwId=GetMainProcess();
	LOG4_INFO("KillQServer pid=%d",dwId);
	if(dwId){
		SetMainProcess(0);
		HANDLE handle=OpenProcess(PROCESS_TERMINATE,FALSE,dwId);
		if(handle!=NULL)
		{
			TerminateProcess(handle,0);
			CloseHandle(handle);
		}
		else
			LOG4_INFO("KillQServer failed pid=%d",dwId);
		ret=TRUE;
	}
	ClearHandle();
	return dwId;
}

void  SetMainProcess(DWORD dwProcessId)
{
	GetCacheData()->dwMainProcessId=dwProcessId;
}
DWORD  GetMainProcess()
{
	return GetCacheData()->dwMainProcessId;
}
void  AddStartupCount()
{
	GetCacheData()->StartupCount++;
}
int  GetStartupCount()
{
	return GetCacheData()->StartupCount;
}
void  ClearStartupCount()
{
	GetCacheData()->StartupCount=0;
}
void  AddCrashCount()
{
	GetCacheData()->CrashCount++;
}
int  GetCrashCount()
{
	return GetCacheData()->CrashCount;
}

void  SetBinDir(TCHAR* Dir)
{
    if (_tcslen(Dir) >= 1024)
        return;

    TCHAR path[1024];

    _tcscpy_s(path, Dir);
    PathAddBackslash(path);
    _tcscpy_s(GetCacheData()->BinDir, path);

    PathRemoveBackslash(path);
    TCHAR* p = _tcsrchr(path, _T('\\'));
    if (p)
        *(p + 1) = _T('\0');

    _tcscpy_s(GetCacheData()->RootDir, path);
}

void  SetClientDir(TCHAR* Dir)
{
    if (_tcslen(Dir) >= 1024)
        return;

    TCHAR path[1024];
    _tcscpy_s(path, Dir);

    PathAddBackslash(path);
    _tcscpy_s(GetCacheData()->ClientDir, path);

    PathRemoveBackslash(path);
    TCHAR* p = _tcsrchr(path, _T('\\'));
    if (p)
        *(p + 1) = _T('\0');

    _tcscpy_s(GetCacheData()->RootDir, path);

    PathAppend(path, _T("\\bin\\"));
    _tcscpy_s(GetCacheData()->BinDir, path);

}

const TCHAR*  GetBinDir()
{
	return GetCacheData()->BinDir;
}

const TCHAR*  GetClientDir()
{
	return GetCacheData()->ClientDir;
}

const TCHAR*  GetRootDir()
{
	return GetCacheData()->RootDir;
}
void  SetServerPath(TCHAR* Path)
{
	_tcscpy_s(GetCacheData()->ServerPath, Path);
}

const TCHAR*  GetServerPath()
{
	return GetCacheData()->ServerPath;
}

#endif
