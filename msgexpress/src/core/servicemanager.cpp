#include "servicemanager.h"
#include "command_config.h"
#include "../inc/logger.h"
#include "generatedpublish.h"
#include <time.h>
#include <string>
#include <sstream>
#include <iostream>


ServiceManager::ServiceManager()
{
}
ServiceManager::ServiceManager(string cfgfile)
{
	cfgFilePath=cfgfile;
	if(!serverCfg.LoadFile(cfgfile))
	{   
		LOG4_ERROR("read server xml config file error,path=%s",cfgFilePath.c_str());
	}   
	/*string xml_file = "Command.xml";
	CommandConfig &cconf=CommandConfig::getInstance();
	if(!cconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read command config file error");
	}  */

	mMapService.rehash(0x10000);
	mMapUserSubIdList.rehash(0x10000);
	mMapUserSubInfo.rehash(0x10000);
	mMapSubDetails.rehash(0x10000);
	mMapServiceRent.rehash(0x4FF);

	loadMasterSlaveService();
	if(serverCfg.connectRedis)
	{
		/*LocalCache::Initialize();
		if(MemCache::Initialize()==0)
		{
			int count=0;
			while(!MemCache::IsConnected())
			{
				sp_sleep(100);
				count++;
				if(count>10)
					break;
			}
		}
		if(MemCache::IsConnected())
		{
			LOG4_INFO( "Connect to Storage service success,db=%d",serverCfg.storage_db);
			MemCache::Select(serverCfg.storage_db);
			MemCache::ClearDB();
		}
		else
			LOG4_ERROR( "Connect to Storage service failed.");*/
	}

	/*thread.detach();
	sp_thread_t thread;
	int ret = sp_thread_create( &thread, NULL, fileMonitor, this );
	if( 0 == ret ) {
		LOG4_INFO( "Thread #%ld has been created to monitor file", thread );
	} else {
		LOG4_ERROR( "Unable to create a thread to monitor file, %s", strerror( errno ) );
	}*/
}
ServiceManager::~ServiceManager()
{
	mMapService.clear();
	mMapServiceProxy.clear();
	mMapUserSubIdList.clear();
}
void ServiceManager::LogCallback(string level,string log)
{
}
int ServiceManager::getRecvQueueSize()
{
	return serverCfg.recvqueuesize;
}
sp_thread_result_t SP_THREAD_CALL  ServiceManager::fileMonitor(void* arg)
{
	//((ServiceManager*) arg)->checkFile();
	return 0;
}
void ServiceManager::checkFile()
{
	int count=0;
	while(1)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		count++;
		if(count>=10)
		{
			count=0;
			ServerConfig cfg;
			if(!cfg.LoadFile(cfgFilePath))
			{   
				LOG4_ERROR("read server xml config file error,path=%s",cfgFilePath.c_str());
			}   
			if(cfg.mastrslaveservice.compare(serverCfg.mastrslaveservice)!=0)
			{
				serverCfg.mastrslaveservice=cfg.mastrslaveservice;
				loadMasterSlaveService();
			}
		}
	}
}
void ServiceManager::loadMasterSlaveService()
{
	char buf[12];
	int pos=0;
	std::unique_lock<std::mutex> lock(mMutexService);
	mMapMasterSlaveService.clear();
	while(pos<serverCfg.mastrslaveservice.size())
	{
	    int ret=sscanf(serverCfg.mastrslaveservice.c_str()+pos , "%[^ ]" , buf);
		if(ret && strlen(buf)>0)
		{
			int serviceid=atoi(buf);
		    mMapMasterSlaveService[serviceid]=true;
			pos+=(strlen(buf)+1);
		}
		else
			pos+=1;
	}
}
string ServiceManager::getAuthData()
{ 
	return serverCfg.auth;
}
AppAddress ServiceManager::getAddress(string uuid)
{
	AppAddress addr = 0;
	std::unique_lock<std::mutex> lock(mMutexService);
	for (auto item : mMapAppInfo)
	{
		if (item.second->logininfo().uuid() == uuid)
		{
			addr = item.first;
			break;
		}
	}
	return addr;
}

string ServiceManager::getServicesInfo()
{
	string ret = "Service list:";
	std::unique_lock<std::mutex> lock(mMutexService);
	for (auto service : mMapService)
	{
		int serviceId = service.first;
		char buf[32];
		memset(buf, 0, 32);
		sprintf(buf, "%d{", serviceId);
		ret.append(buf);
		vector<AppAddress>* list = service.second;
		if (list == nullptr)
			continue;
		for (auto item : *list)
		{
			ret += item.toString();
			ret += ",";
		}
		ret += "};";
	}
	for (auto serviceproxy : mMapServiceProxy)
	{
		int serviceId = serviceproxy.first;
		char buf[32];
		memset(buf, 0, 32);
		sprintf(buf, "%d*{", serviceId);
		ret.append(buf);
		vector<AppAddress>* list = serviceproxy.second;
		if (list == nullptr)
			continue;
		for (auto item : *list)
		{
			ret += item.toString();
			ret += ",";
		}
		ret += "};";
	}
	return ret;
}

void ServiceManager::regService(vector<int>& vecService, AppAddress addr)
{
	std::unique_lock<std::mutex> lock(mMutexService);
	MsgExpress::AppInfo* info=mMapAppInfo[addr];
	if(!info)
	{
		LOG4_ERROR("No app info,why? addr=%s",addr.toString().c_str());
		return;
		info=new MsgExpress::AppInfo();
		mMapAppInfo[addr]=info;
		LOG4_ERROR("No app info,why? addr=%s",addr.toString().c_str());
	}
	for (auto serviceId : vecService)
	{
		if(info->logininfo().type()==3)
		{
			vector<AppAddress>* list = mMapServiceProxy[serviceId];
			if(list==nullptr)
			{
				list = new vector<AppAddress>();
				mMapServiceProxy[serviceId]=list;
			}
			list->push_back(addr);
		}
		else
		{
			vector<AppAddress>* list = mMapService[serviceId];
			if(list==nullptr)
			{
				list = new vector<AppAddress>();
				mMapService[serviceId]=list;
			}
			list->push_back(addr);
		}
		LOG4_INFO("Register service %d : %s", serviceId, addr.toString().c_str());
	}
}

void ServiceManager::unregService(std::unordered_set<int>& setService, AppAddress addr)
{
	LOG4_INFO("ServiceManager::unregService");
	std::unique_lock<std::mutex> lock(mMutexService);
	vector<AppAddress>* renterlist = nullptr;
	unordered_map<AppAddress, vector<AppAddress>*, AppAddressHash>::const_iterator rentlistIter = mMapServiceRenterList.find(addr);
	if(rentlistIter!=mMapServiceRenterList.end())
		renterlist=rentlistIter->second;

	for (auto  serviceId : setService)
	{
		vector<AppAddress>* list = nullptr;
		if (mMapService.find(serviceId) != mMapService.end())
			list = mMapService[serviceId];
		vector<AppAddress>* list2 = nullptr;
		if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end())
			list2 = mMapServiceProxy[serviceId];
		if (list == nullptr && list2 == nullptr) 
			continue;
		if (list)
		{
			for (int i = 0; i < list->size(); i++)
			{
				if (list->at(i) == addr)
				{
					list->erase(list->begin() + i);
					LOG4_INFO("Unregister service %d : %s", serviceId, addr.toString().c_str());
					break;
				}
			}
		}
		if (list2)
		{
			for (int i = 0; i < list2->size(); i++)
			{
				if (list2->at(i) == addr)
				{
					list2->erase(list2->begin() + i);
					LOG4_INFO("Unregister service %d : %s", serviceId, addr.toString().c_str());
					break;
				}
			}
		}
		//remove service renter
		if(renterlist)
		{
			for(int i=0;i<renterlist->size();i++)
			{
				AppAddress id = renterlist->at(i);
				uint64_t key=((uint64_t)addr.Id<<32)+serviceId;
				mMapServiceRent.erase(key);
			}
		}
	}
	delete renterlist;
	mMapServiceRenterList.erase(addr);
}
AppAddress ServiceManager::getServiceProvider(int serviceId, AppAddress srcAddr, MsgExpress::LoginInfo& info, unsigned short code)
{
	AppAddress sid(0);
	if(mMapMasterSlaveService.find(serviceId)!=mMapMasterSlaveService.end())
	{
		LOG4_DEBUG("Loadbalance type=Master-Slave,service=%d",serviceId);
		std::unique_lock<std::mutex> lock(mMutexService);
		if (mMapService.find(serviceId) != mMapService.end() && mMapService[serviceId]->size()>0)
		{
			sid=mMapService[serviceId]->at(0);
		}
		else if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end() && mMapServiceProxy[serviceId]->size()>0)
		{
			if(info.type()!=3)
				sid=mMapServiceProxy[serviceId]->at(0);
		}
	    return sid;
	}
	else if(code!=0)
	{
		LOG4_DEBUG("Loadbalance type=Hashcode,service=%d,code=%d",serviceId,code);
		std::unique_lock<std::mutex> lock(mMutexService);
		if (mMapService.find(serviceId) != mMapService.end() && mMapService[serviceId]->size()>0)
		{
			sid = mMapService[serviceId]->at(code % mMapService[serviceId]->size());
		}
		else if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end() && mMapServiceProxy[serviceId]->size()>0)
		{
			if(info.type()!=3)
				sid = mMapServiceProxy[serviceId]->at(code % mMapServiceProxy[serviceId]->size());
		}
	    return sid;
	}
	else
	{
		LOG4_DEBUG("Loadbalance type=Rent,service=%d,addr=%s", serviceId, srcAddr.toString().c_str());
		std::unique_lock<std::mutex> lock(mMutexService);
		if (mMapService.find(serviceId) != mMapService.end() && mMapService[serviceId]->size()>0)
		{
			uint64_t key=((uint64_t)srcAddr<<32)+serviceId;
			if (mMapServiceRent.find(key) != mMapServiceRent.end() && serverCfg.routemode == 0)
			{
				sid = mMapServiceRent[key];
			}
			else{
				unsigned int index = mMapLoadbalance[serviceId];
				index = (index + 1) % mMapService[serviceId]->size();
				sid = mMapService[serviceId]->at(index);
				bool find = false;
				if (sid.Id == srcAddr)
				{
					for (int i = 0; i < mMapService[serviceId]->size(); i++)
					{
						if (mMapService[serviceId]->at(i) != (AppAddress)srcAddr)
						{
							sid = mMapService[serviceId]->at(i);
							mMapLoadbalance[serviceId] = i;
							find = true;
							break;
						}
					}
				}
				else
				{
					mMapLoadbalance[serviceId] = index;
					find = true;
				}
				if (!find)
				{
					if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end() && mMapServiceProxy[serviceId]->size()>0)
					{
						if (info.type() != 3)
							sid = mMapServiceProxy[serviceId]->at(0);
					}
				}
				//下面的注释后就变成轮流分配了
				/*mMapServiceRent[key]=sid;
				UserList* list=mMapServiceRenterList[sid];
				if(list==NULL)
				{
					list=new UserList();
					mMapServiceRenterList[sid]=list;
				}
				list->add(srcAddr);*/
			}
		}
		else if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end() && mMapServiceProxy[serviceId]->size()>0)
		{
			if(info.type()!=3)
				sid=mMapServiceProxy[serviceId]->at(0);
		}
		return sid;
	}
}
void ServiceManager::getServiceProviderList(int serviceId, std::vector<AppAddress>& list, std::vector<AppAddress>& proxylist){
	std::unique_lock<std::mutex> lock(mMutexService);
	if (mMapService.find(serviceId) != mMapService.end())
		list.assign(mMapService[serviceId]->begin(), mMapService[serviceId] ->end());
	if (mMapServiceProxy.find(serviceId) != mMapServiceProxy.end())
		proxylist.assign(mMapServiceProxy[serviceId]->begin(), mMapServiceProxy[serviceId]->end());
}
void ServiceManager::regApp(AppAddress addr, const MsgExpress::LoginInfo& loginInfo)
{
	std::unique_lock<std::mutex> lock(mMutexService);
	MsgExpress::AppInfo* info = NULL;
	if (mMapAppInfo.find(addr) == mMapAppInfo.end())
	{
		info=new MsgExpress::AppInfo();
		mMapAppInfo[addr]=info;
	}
	else
		info = mMapAppInfo[addr];
	MsgExpress::LoginInfo* pLogin=new MsgExpress::LoginInfo(loginInfo);
	info->set_allocated_logininfo(pLogin);
	time_t tm;
	time(&tm);
	info->set_logintime(tm);
	info->set_addr(addr);
	mMapCountInfo[addr]=new CountInfo();

	if(serverCfg.connectRedis)
	{
		string data=info->SerializeAsString();
		char key[24];
		sprintf(key,"%d",info->addr());
		string errstr;

		//int ret=LocalCache::Set(key,data,&errstr);
		///if(ret)
		//	LOG4_ERROR("Set memcache failed,addr=%d,err:%s",info->addr(),errstr.c_str());
	}
}
void ServiceManager::unregApp(AppAddress addr)
{
	bool found=false;
	LOG4_INFO("ServiceManager::unregApp");
	std::unique_lock<std::mutex> lock(mMutexService);
	unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash>::iterator it = mMapAppInfo.find(addr);
	if(it!=mMapAppInfo.end())
	{
		MsgExpress::AppInfo* tmp=it->second;
		mMapAppInfo.erase(it);
		delete tmp;
		found=true;
	}

	unordered_map<AppAddress, CountInfo*, AppAddressHash>::iterator iter = mMapCountInfo.find(addr);
	if(iter!=mMapCountInfo.end())
	{
		CountInfo* tmp=iter->second;
		mMapCountInfo.erase(iter);
		delete tmp;
		
	}
}
bool ServiceManager::getAppInfo(AppAddress addr, MsgExpress::AppInfo& appInfo)
{
	bool ret=false;
	//LOG4_INFO("ServiceManager::getAppInfo");
	std::unique_lock<std::mutex> lock(mMutexService);
	unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash>::iterator it = mMapAppInfo.find(addr);
	if (it != mMapAppInfo.end())
	{
		MsgExpress::AppInfo* info = it->second;
		if (info)
		{
			appInfo.CopyFrom(*info);
			ret = true;
		}
	}
	return ret;
}
void ServiceManager::getAppType(std::vector<AppAddress>& vecAddr, std::vector<int>& vecType)
{
	vecType.clear();
	std::unique_lock<std::mutex> lock(mMutexService);
	for (auto addr : vecAddr)
	{
		unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash>::iterator it = mMapAppInfo.find(addr);
		if(it!=mMapAppInfo.end())
			vecType.push_back(it->second->logininfo().type());
		else
			vecType.push_back(-1);
	}
}
bool ServiceManager::getCountInfo(AppAddress addr, CountInfo& countInfo)
{
	bool ret=false;
	std::unique_lock<std::mutex> lock(mMutexCount);
	unordered_map<AppAddress, CountInfo*, AppAddressHash>::iterator it = mMapCountInfo.find(addr);
	if(it!=mMapCountInfo.end())
	{
		countInfo=*it->second;
		ret=true;
	}
	return ret;
}
void ServiceManager::updateAppStatus(const MsgExpress::UpdateAppStatus& info)
{
	std::unique_lock<std::mutex> lock(mMutexService);
	unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash>::iterator it = mMapAppInfo.find(info.addr());
	if(it!=mMapAppInfo.end())
	{
		it->second->set_status(info.status());
		it->second->set_token(info.token());
	}
}
void ServiceManager::updateCountInfo(AppAddress addr, const CountInfo& countInfo)
{
	std::unique_lock<std::mutex> lock(mMutexCount);
	unordered_map<AppAddress, CountInfo*, AppAddressHash>::iterator it = mMapCountInfo.find(addr);
	if(it!=mMapCountInfo.end())
	{
		if(countInfo.inPublish>0)
			it->second->inPublish+=countInfo.inPublish;
		if(countInfo.outPublish>0)
			it->second->outPublish+=countInfo.outPublish;
		if(countInfo.inRequest>0)
			it->second->inRequest+=countInfo.inRequest;
		if(countInfo.outRequest>0)
			it->second->outRequest+=countInfo.outRequest;
		if(countInfo.inResponse>0)
			it->second->inResponse+=countInfo.inResponse;
		if(countInfo.outResponse>0)
			it->second->outResponse+=countInfo.outResponse;
		if(countInfo.inPublishB>0)
			it->second->inPublishB+=countInfo.inPublishB;
		if(countInfo.outPublishB>0)
			it->second->outPublishB+=countInfo.outPublishB;
		if(countInfo.inRequestB>0)
			it->second->inRequestB+=countInfo.inRequestB;
		if(countInfo.outRequestB>0)
			it->second->outRequestB+=countInfo.outRequestB;
		if(countInfo.inResponseB>0)
			it->second->inResponseB+=countInfo.inResponseB;
		if(countInfo.outResponseB>0)
			it->second->outResponseB+=countInfo.outResponseB;
		if(countInfo.queueLength>0)
			it->second->queueLength=countInfo.queueLength;
	}
}

void ServiceManager::getAppList(MsgExpress::AppList& result)
{
	std::unique_lock<std::mutex> lock(mMutexService);
	unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash>::const_iterator it = mMapAppInfo.begin();
	for (auto item : mMapAppInfo)
	{
		MsgExpress::AppInfo* info = result.add_appinfo();
		info->CopyFrom(*item.second);
	}
}

void ServiceManager::getAppInfo(const MsgExpress::GetAppList& request,MsgExpress::AppList& result)
{
	if(request.serviceid_size()==0)
	{
		getAppList(result);
	}
	else
	{
		std::unique_lock<std::mutex> lock(mMutexService);
		for(int i=0;i<request.serviceid_size();i++)
		{
			int id=request.serviceid(i);
			std::vector<AppAddress>* uList = NULL;
			if (mMapService.find(id) != mMapService.end())
				uList = mMapService[id];
			if(!uList)
				continue;
			for (auto addr : *uList)
			{
				if (mMapAppInfo.find(addr) == mMapAppInfo.end())
					continue;
				MsgExpress::AppInfo* info = mMapAppInfo[addr];
				if(!info)
					continue;
				MsgExpress::AppInfo* appInfo=result.add_appinfo();
				appInfo->CopyFrom(*info);
			}
		}
	}
}
void sort(MsgExpress::DataItem* subInfo,int count)
{
	if(count<2) return;
	for(int i=1;i<count;i++)
	{
		for(int j=i;j>0;j--)
		{
			if(subInfo[j].key()<subInfo[j-1].key())
			{
				MsgExpress::DataItem temp(subInfo[j-1]);
				subInfo[j-1]=subInfo[j];
				subInfo[j]=temp;
			}
		}
	}
}
int getSortedSub(const MsgExpress::SubscribeData& sub,MsgExpress::DataItem*& subInfo)
{
	subInfo=NULL;
	int count=0;
	for(int i=0;i<sub.condition_size();i++)
	{
		if(sub.condition(i).key()!=0)
			count++;
	}
	if(count<1)
		return count;
	subInfo=new MsgExpress::DataItem[count];
	count=0;
	for(int i=0;i<sub.condition_size();i++)
	{
		if(sub.condition(i).key()!=0)
		{
			subInfo[count]=sub.condition(i);
			count++;
		}
	}
	sort(subInfo,count);
	return count;
}
int getSortedPub(MsgExpress::PublishData* pub,const set<int>& keys,MsgExpress::DataItem*& subInfo)
{
	subInfo=NULL;
	int count=0;
	for(int i=0;i<pub->item_size();i++)
	{
		if(keys.find(pub->item(i).key())!=keys.end())
			count++;
	}
	if(count<1)
		return count;
	subInfo=new MsgExpress::DataItem[count];
	int total=0;
	for(int i=0;i<pub->item_size();i++)
	{
		if(keys.find(pub->item(i).key())!=keys.end())
		{
			subInfo[total]=pub->item(i);
			//subInfo[total].add_value("");
			total++;
			if(total>=count)
				break;
		}
	}
	sort(subInfo,count);
	return count;
}
void generateSubConditions(int topic,MsgExpress::DataItem* subInfo,int count,bool isPub,vector<string>& vecResult)
{
	vector<string> vecTemp;
	std::stringstream stream;
	stream << "T" << topic ;
	vecResult.clear();
	vecResult.push_back(stream.str());
	int circle = 0;
	for(int i=0;i<count;i++)
	{
		const MsgExpress::DataItem& item=subInfo[i];
		int key=item.key();
		for(size_t k=0;k<vecResult.size();k++)
		{
			circle++;
			if (item.strval_size()>0)
			{
				for(int j=0;j<item.strval_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					if (item.strval(j).size()>0 && item.strval(j).size()<60)
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.strval(j).c_str();
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.ival_size())
			{
				for(int j=0;j<item.ival_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.ival(j);
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.uival_size())
			{
				for(int j=0;j<item.uival_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.uival(j);
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.lval_size())
			{
				for(int j=0;j<item.lval_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.lval(j);
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.ulval_size())
			{
				for(int j=0;j<item.ulval_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.ulval(j);
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.bval_size())
			{
				for(int j=0;j<item.bval_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.bval(j);
						vecTemp.push_back(stream.str());
					}
				}
			}
			else if (item.compval_size())
			{
				for(int j=0;j<item.compval_size();j++)
				{
					if(isPub && j==0)
						vecTemp.push_back(vecResult[k]);
					
					{
						stream.str("");
						stream << vecResult[k].c_str() << '&' << key << '=' << item.compval(j).a();
						vecTemp.push_back(stream.str());
					}
				}
			}
		}
		vecResult.clear();
		vecResult=vecTemp;
		vecTemp.clear();
		if (circle > 500)
			break;
	}
}

void ServiceManager::subscribe(int topic, int subid, AppAddress addr, const vector<string>& vecCondition, const vector<int>& keys, MessagePtr subMsg)
{
	char subKey[32];
	memset(subKey,0,sizeof(subKey));
	sprintf(subKey,"%d:%d",(unsigned int)addr,subid);
	bool hasSubed = false;
	{
		std::unique_lock<std::mutex> lock(mMutexSub);
		hasSubed = mMapUserSubInfo[subKey] != NULL ? true : false;
	}
	if(hasSubed)
		unSubscribe(subid,addr);
	std::unique_lock<std::mutex> lock(mMutexSub);
	
	set<int>& setKeys=mMapTopicKeys[topic];
	for(int i=0;i<keys.size();i++)
		setKeys.insert(keys[i]);

	vector<int>* subIds=mMapUserSubIdList[addr];
	if(subIds==NULL){
		subIds=new vector<int>;
		mMapUserSubIdList[addr]=subIds;
	}
	subIds->push_back(subid);
	mMapUserSubInfo[subKey]=subMsg;
	for(size_t i=0;i<vecCondition.size();i++)
	{
	    MapSubscription* subscription=mMapSubDetails[vecCondition[i]];
		if(subscription==NULL){
			subscription=new MapSubscription();
			subscription->rehash(0x400);
			mMapSubDetails[vecCondition[i]]=subscription;
		}
		vector<int>* userSubIds=(*subscription)[addr];
		if(userSubIds==NULL)
		{
			userSubIds=new vector<int>();
			(*subscription)[addr]=userSubIds;
		}
		userSubIds->push_back(subid);
	}
}
void ServiceManager::subscribe(const MsgExpress::SubscribeData& sub, AppAddress addr)
{
	MsgExpress::DataItem* subInfo=NULL;
	int count=getSortedSub(sub,subInfo);
	vector<int> keys;
	for(int i=0;i<count;i++)
		keys.push_back(subInfo[i].key());
	vector<string> vecResult;
	generateSubConditions(sub.topic(),subInfo,count,false,vecResult);
	delete []subInfo;
	MsgExpress::SubscribeData* copySub=new MsgExpress::SubscribeData(sub);
	subscribe(sub.topic(),sub.subid(),addr,vecResult,keys,MessagePtr(copySub));
}
void ServiceManager::subscribe(const MsgExpress::SimpleSubscription& sub, AppAddress addr)
{
	vector<int> keys;
	vector<string> vecTotal;
	for(int i=0;i<sub.submsg_size();i++)
	{
		string subData=sub.submsg(i);
		GeneratedPublish generared;
		generared.ParseFromString(subData);
		MsgExpress::DataItem* subInfo=NULL;
		int count=generared.GetSubInfo(subInfo);
		for(int j=0;j<count;j++)
		    keys.push_back(subInfo[j].key());
		vector<string> vecResult;
		generateSubConditions(sub.topic(),subInfo,count,false,vecResult);
		for(int j=0;j<vecResult.size();j++)
			vecTotal.push_back(vecResult[j]);
		delete []subInfo;
	}
	MsgExpress::SimpleSubscription* copySub=new MsgExpress::SimpleSubscription(sub);
	subscribe(sub.topic(),sub.subid(),addr,vecTotal,keys,MessagePtr(copySub));
}
void ServiceManager::unSubscribe(int subId, AppAddress addr)
{
	char subKey[32];
	memset(subKey,0,sizeof(subKey));
	sprintf(subKey, "%d:%d", (unsigned int)addr, subId);
	MessagePtr subMsg = nullptr;
	{
		std::unique_lock<std::mutex> lock(mMutexSub);
		subMsg = mMapUserSubInfo[subKey];
	}
	if(!subMsg)
		return;
	int topic=0;
	
	vector<string> vecResult;
	if(subMsg->GetTypeName().compare(MsgExpress::SubscribeData::default_instance().GetTypeName())==0)
	{
	    MsgExpress::SubscribeData* sub=(MsgExpress::SubscribeData*)mMapUserSubInfo[subKey].get();
		topic=sub->topic();
		MsgExpress::DataItem* subInfo=NULL;
		int count=getSortedSub(*sub,subInfo);
		generateSubConditions(sub->topic(),subInfo,count,false,vecResult);
		delete []subInfo;
	}
	else if(subMsg->GetTypeName().compare(MsgExpress::SimpleSubscription::default_instance().GetTypeName())==0)
	{
	    MsgExpress::SimpleSubscription* sub=(MsgExpress::SimpleSubscription*)mMapUserSubInfo[subKey].get();
		topic=sub->topic();
		for(int i=0;i<sub->submsg_size();i++)
		{
			string subData=sub->submsg(i);
			GeneratedPublish generared;
			generared.ParseFromString(subData);
			MsgExpress::DataItem* subInfo=NULL;
			int count=generared.GetSubInfo(subInfo);
			vector<string> vecTmp;
			generateSubConditions(topic,subInfo,count,false,vecTmp);
			for(int j=0;j<vecTmp.size();j++)
				vecResult.push_back(vecTmp[j]);
			delete []subInfo;
		}
	}

	std::unique_lock<std::mutex> lock(mMutexSub);
	mMapUserSubInfo.erase(mMapUserSubInfo.find(subKey));
	vector<int>* subIds=mMapUserSubIdList[addr];
	if(subIds!=nullptr){
		vector<int>::iterator it=subIds->begin();
		while(it!=subIds->end())
		{
			if(*it==subId)
			{
				subIds->erase(it);
				break;
			}
			it++;
		}
		if(subIds->size()==0)
		{
		    delete subIds;
		    mMapUserSubIdList.erase(mMapUserSubIdList.find(addr));
		}
	}
	for(size_t i=0;i<vecResult.size();i++)
	{
	    MapSubscription* subscription=mMapSubDetails[vecResult[i]];
		if(subscription!=NULL){
			vector<int>* userSubIds=(*subscription)[addr];
			if(userSubIds!=NULL)
			{
				vector<int>::iterator it=userSubIds->begin();
				while(it!=userSubIds->end())
				{
					if(*it==subId)
					{
						userSubIds->erase(it);
						break;
					}
					it++;
				}
				if(userSubIds->size()==0)
				{
					MapSubscription::iterator iter=subscription->find(addr);
					subscription->erase(iter);
					delete userSubIds;
				}
			}
		}
	}
	
}
void ServiceManager::unSubscribeAll(AppAddress addr)
{
	LOG4_INFO("ServiceManager::unSubscribeAll");
	vector<int> vecTemp;
	std::unique_lock<std::mutex> lock(mMutexSub);
	vector<int>* subIds=mMapUserSubIdList[addr];
	lock.unlock();
	if(subIds!=NULL){
		vecTemp=*subIds;
	}
	vector<int>::iterator it=vecTemp.begin();
	while(it!=vecTemp.end())
	{
		unSubscribe(*it,addr);
		it++;
	}
}
bool ServiceManager::IsMyFood(MsgExpress::PublishData* pubData, AppAddress addr)
{
	bool ret=false;
	int topic=pubData->topic();
	MsgExpress::DataItem* subInfo=NULL;
	std::unique_lock<std::mutex> lock(mMutexSub);
	set<int>& keys = mMapTopicKeys[topic];
	int count = getSortedPub(pubData, keys, subInfo);
	lock.unlock();
	vector<string> vecResult;
	generateSubConditions(topic,subInfo,count,true,vecResult);
	delete []subInfo;
	lock.lock();
	for(size_t i=0;i<vecResult.size();i++)
	{
	    MapSubscription* subscription=mMapSubDetails[vecResult[i]];
		if(subscription==NULL) continue;
		if(subscription->size()>0)
		{
			ret=true;
			break;
		}
	}
	return ret;
}
void ServiceManager::getSubscriberList(MsgExpress::PublishData* pubData, unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash>& mapUserSubList){
	int topic=pubData->topic();
	MsgExpress::DataItem* subInfo=nullptr;
	std::unique_lock<std::mutex> lock(mMutexSub);
	set<int>& keys = mMapTopicKeys[topic];
	int count = getSortedPub(pubData, keys, subInfo);
	lock.unlock();
	vector<string> vecResult;
	generateSubConditions(topic,subInfo,count,true,vecResult);
	delete []subInfo;
	lock.lock();
	for (auto result : vecResult)
	{
		if (mMapSubDetails.find(result) == mMapSubDetails.end())
			continue;
		MapSubscription* subscription = mMapSubDetails[result];
		for (auto item : *subscription)
		{
			AppAddress addr = item.first;
			vector<MsgExpress::SubscribeData*>* vecSubData = nullptr;
			if (mapUserSubList.find(addr) == mapUserSubList.end())
			{
				vecSubData=new vector<MsgExpress::SubscribeData*>();
				mapUserSubList[addr] = vecSubData;
			}
			else
				vecSubData = mapUserSubList[addr];
			for (auto subId : *item.second)
			{
				char subKey[32];
	            memset(subKey,0,32);
				sprintf(subKey, "%d:%d", (unsigned int)addr, subId);
				MsgExpress::SubscribeData* subData=(MsgExpress::SubscribeData*)mMapUserSubInfo[subKey].get();
				if(subData)
					vecSubData->push_back(new MsgExpress::SubscribeData(*subData));
			}
		}
	}
}
unordered_set<AppAddress, AppAddressHash>* ServiceManager::getSubscriberIdList(int topic, const string& pubMsg)
{
#ifdef _WIN32
	static __declspec(thread) unordered_set<AppAddress, AppAddressHash>* setIds = nullptr;
#else
	static __thread unordered_set<AppAddress, AppAddressHash>* setIds=NULL;
#endif
	if(setIds==nullptr)
	{
		setIds = new unordered_set<AppAddress, AppAddressHash>();
		LOG4_INFO("TLS thread=%d,obj=0x%x\r\n",sp_thread_self(),setIds);
	}
	setIds->clear();


	
	std::unique_lock<std::mutex> lock(mMutexSub);
	MsgExpress::DataItem* subInfo = nullptr;
	set<int>& keys = mMapTopicKeys[topic];
	int count = 0;
	if (keys.size() > 0)
	{
		GeneratedPublish generared;
		generared.ParseFromString(pubMsg);
		count = generared.GetSubInfo(subInfo, keys);
	}
	lock.unlock();
	vector<string> vecResult;
	generateSubConditions(topic,subInfo,count,true,vecResult);
	delete []subInfo;

	lock.lock();
	for (auto result : vecResult)
	{
		MapSubscription* subscription = mMapSubDetails[result];
		if (subscription == nullptr) continue;
		for (auto item : *subscription)
		{
			AppAddress userId = item.first;
			setIds->insert(userId);
		}
	}
	return setIds;
}

unordered_set<AppAddress, AppAddressHash>* ServiceManager::getSubscriberIdList(MsgExpress::PublishData* pubData)
{
#ifdef _WIN32
	static __declspec(thread) unordered_set<AppAddress, AppAddressHash>* setIds = nullptr;
#else
	static __thread unordered_set<AppAddress, AppAddressHash>* setIds=NULL;
#endif
	if(setIds==nullptr)
	{
		setIds = new unordered_set<AppAddress, AppAddressHash>();
		LOG4_INFO("TLS thread=%d,obj=0x%x\r\n",sp_thread_self(),setIds);
	}
	setIds->clear();

	int topic=pubData->topic();
	MsgExpress::DataItem* subInfo = nullptr;
	std::unique_lock<std::mutex> lock(mMutexSub);
	set<int>& keys = mMapTopicKeys[topic];
	int count=getSortedPub(pubData,keys,subInfo);
	lock.unlock();
	vector<string> vecResult;
	generateSubConditions(topic,subInfo,count,true,vecResult);
	delete []subInfo;
	lock.lock();
	for (auto result : vecResult)
	{
	    MapSubscription* subscription=mMapSubDetails[result];
		if(subscription==nullptr) continue;
		for (auto item : *subscription)
		{
			AppAddress userId = item.first;
			setIds->insert(userId);
		}
	}
	return setIds;
}
bool ServiceManager::isMasterSlaveMode(unsigned int serviceId)
{
	return mMapMasterSlaveService.find(serviceId) != mMapMasterSlaveService.end() && mMapMasterSlaveService[serviceId];
}
std::vector<int> ServiceManager::getMasterSlaveService()
{
	std::vector<int> list;
	for (auto item : mMapMasterSlaveService)
	{
		list.push_back(item.first);
	}
	return list;
}
