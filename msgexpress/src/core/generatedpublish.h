#pragma once

#include <map>
#include <hash_map>
#include <set>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <google/protobuf/stubs/common.h>
#include "google/protobuf/io/coded_stream.h"

#include "../inc/protobuf.h"

using namespace std;

class GeneratedPublish
{
private:
	std::vector<int> vecFields;
	std::unordered_map<int,std::vector<std::string>*> mapValues;

private:
	std::vector<std::string>* GetVector(int fieldNum);
public:
	GeneratedPublish(void);
	~GeneratedPublish(void);
	void ParseFromCodedStream(google::protobuf::io::CodedInputStream* input);
	void ParseFromString(const std::string& input);
	int GetSubInfo(MsgExpress::DataItem*& subInfo);
	int GetSubInfo(MsgExpress::DataItem*& subInfo, const set<int>& keys);
};

