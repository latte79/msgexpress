

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#include "ioutils.h"
#include "../inc/logger.h"
#include "../core/porting.h"

void IOUtils::inetNtoa(in_addr * addr, char * ip, int size)
{
#ifdef WIN32
	snprintf( ip, size, "%i.%i.%i.%i", addr->s_net, addr->s_host, addr->s_lh, addr->s_impno );
#else
	const unsigned char *p = ( const unsigned char *) addr;
	snprintf( ip, size, "%i.%i.%i.%i", p[0], p[1], p[2], p[3] );
#endif
}

int IOUtils::setNonblock(int fd)
{
#ifdef WIN32
	unsigned long nonblocking = 1;
	ioctlsocket( fd, FIONBIO, (unsigned long*) &nonblocking );
#else
	int flags;

	flags = fcntl( fd, F_GETFL );
	if( flags < 0 ) return flags;

	flags |= O_NONBLOCK;
	if( fcntl( fd, F_SETFL, flags ) < 0 ) return -1;
#endif

	return 0;
}

int IOUtils::setBlock(int fd)
{
#ifdef WIN32

	unsigned long nonblocking = 0;
	ioctlsocket( fd, FIONBIO, (unsigned long*) &nonblocking );

#else

	int flags;

	flags = fcntl( fd, F_GETFL );
	if( flags < 0 ) return flags;

	flags &= ~O_NONBLOCK;
	if( fcntl( fd, F_SETFL, flags ) < 0 ) return -1;
#endif

	return 0;
}

int IOUtils::setNodelay(int fd)
{
	int ret = 0;
#ifdef WIN32
	int flags = 1;
	if( setsockopt( fd, IPPROTO_TCP, TCP_NODELAY, (char*)&flags, sizeof(flags) ) < 0 ) {
		LOG4_ERROR("failed to set socket to nodelay");
		ret = -1;
	}
#else

#endif
	return ret;
}

int IOUtils::setBuffer(intptr_t fd, unsigned int sndbuf, unsigned int recvbuf)
{
	int ret=0;
	if( 0 == ret )
	{
		int snd_size = sndbuf; 
		socklen_t optlen = sizeof(snd_size); 
		int err = setsockopt(fd, SOL_SOCKET, SO_SNDBUF,(char*) &snd_size, optlen); 
		if(err<0){ 
			LOG4_ERROR("set send buf size error");
			ret=-1;
		} 
		else
		{
			getsockopt(fd, SOL_SOCKET, SO_SNDBUF,(char*) &snd_size, &optlen); 
			if (snd_size == sndbuf)
			{
				LOG4_INFO("set send buf size :%d,fd=%d", snd_size, fd);
			}
			else
			{
				LOG4_ERROR("set send buf size :%d,fd=%d", snd_size, fd);
			}
		}
	}
	if( 0 == ret )
	{
		//set recv buf size
		int rcv_size = recvbuf;    
		socklen_t optlen = sizeof(rcv_size); 
		int err = setsockopt(fd,SOL_SOCKET,SO_RCVBUF, (char *)&rcv_size, optlen); 
		if(err<0){ 
			LOG4_ERROR("set recv buf size error");
			ret=-1;
		}
		else
		{
			getsockopt(fd, SOL_SOCKET, SO_RCVBUF,(char*) &rcv_size, &optlen); 
			if (rcv_size == recvbuf)
			{
				LOG4_INFO("set recv buf size :%d , fd=%d", rcv_size, fd);
			}
			else
			{
				LOG4_ERROR("set recv buf size :%d , fd=%d", rcv_size, fd);
			}
		}
	}
	return ret;
}

int IOUtils::setTimeout(intptr_t fd, unsigned char sndtimeout, unsigned char recvtimeout)
{
	int ret = 0;
#ifdef WIN32
	int sndTime = sndtimeout * 1000;
	int rcvTime = recvtimeout * 1000;
	ret = setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (char*)&sndTime, sizeof(sndTime));
	ret = setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&rcvTime, sizeof(rcvTime));
#else
	struct timeval sndTime = { sndtimeout, 0 };
	struct timeval rcvTime = { recvtimeout, 0 };
	//int ret=setsockopt((int)sock_fd,SOL_SOCKET,SO_SNDTIMEO,(const char*)&sndTime,sizeof(sndTime));
	//int ret = setsockopt((int)sock_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&rcvTime, sizeof(rcvTime));
#endif
	return ret;
}

int IOUtils::tcpListen(const char * ip, int port, int * fd, int blocking)
{
	int ret = 0;

	int listenFd = socket( AF_INET, SOCK_STREAM, 0 );
	if( listenFd < 0 ) {
		LOG4_ERROR("socket failed, errno %d, %s", errno, strerror(errno));
		ret = -1;
	}

	if( 0 == ret && 0 == blocking ) {
		if( setNonblock( listenFd ) < 0 ) {
			LOG4_ERROR("failed to set socket to non-blocking");
			ret = -1;
		}
	}

	if( 0 == ret ) {
		int flags = 1;
		if( setsockopt( listenFd, SOL_SOCKET, SO_REUSEADDR, (char*)&flags, sizeof( flags ) ) < 0 ) {
			LOG4_ERROR("failed to set setsock to reuseaddr");
			ret = -1;
		}
		if( setsockopt( listenFd, IPPROTO_TCP, TCP_NODELAY, (char*)&flags, sizeof(flags) ) < 0 ) {
			LOG4_ERROR("failed to set socket to nodelay");
			ret = -1;
		}
	}
	 // set send buf size
	ret=setBuffer(listenFd,1024*1024,1024*1024);

	struct sockaddr_in addr;

	if( 0 == ret ) {
		memset( &addr, 0, sizeof( addr ) );
		addr.sin_family = AF_INET;
		addr.sin_port = htons( port );

		addr.sin_addr.s_addr =INADDR_ANY;
		if( '\0' != *ip ) {
			if( 0 == sp_inet_aton( ip, &addr.sin_addr ) ) {
				LOG4_ERROR("failed to convert %s to inet_addr", ip);
				ret = -1;
			}
		}
	}

	if( 0 == ret ) {
		if( bind( listenFd, (struct sockaddr*)&addr, sizeof( addr ) ) < 0 ) {
			LOG4_ERROR("bind failed, errno %d, %s", errno, strerror(errno));
			ret = -1;
		}
	}

	if( 0 == ret ) {
		if( ::listen( listenFd, 1024 ) < 0 ) {
			LOG4_ERROR("listen failed, errno %d, %s", errno, strerror(errno));
			ret = -1;
		}
	}

	if( 0 != ret && listenFd >= 0 ) sp_close( listenFd );

	if( 0 == ret ) {
		* fd = listenFd;
		LOG4_INFO("Listen on port [%d]", port);
	}

	return ret;
}

int IOUtils::tcpListen(const char * path, int * fd, int blocking, int mode)
{
	int ret = 0;
#ifdef WIN32
	
#else

	struct sockaddr_un addr;
	memset( &addr, 0, sizeof( addr ) );

	if( strlen( path ) > ( sizeof( addr.sun_path ) - 1 ) ) {
		sp_syslog( LOG_WARNING, "UNIX socket name %s too long", path );
		return -1;
	}

	if( 0 == access( path, F_OK ) ) {
		if( 0 != unlink( path ) ) {
			sp_syslog( LOG_WARNING, "unlink %s failed, errno %d, %s",
				path, errno, strerror( errno ) );
			return -1;
		}
	}

	addr.sun_family = AF_UNIX;
	strncpy( addr.sun_path, path, sizeof( addr.sun_path ) - 1 );

	int listenFd = socket( AF_UNIX, SOCK_STREAM, 0 );
	if( listenFd < 0 ) {
		sp_syslog( LOG_WARNING, "listen failed, errno %d, %s", errno, strerror( errno ) );
		ret = -1;
	}

	if( 0 == ret && 0 == blocking ) {
		if( setNonblock( listenFd ) < 0 ) {
			sp_syslog( LOG_WARNING, "failed to set socket to non-blocking" );
			ret = -1;
		}
	}

	if( 0 == ret ) {
		int flags = 1;
		if( setsockopt( listenFd, SOL_SOCKET, SO_REUSEADDR, (char*)&flags, sizeof( flags ) ) < 0 ) {
			sp_syslog( LOG_WARNING, "failed to set setsock to reuseaddr" );
			ret = -1;
		}
	}
	 // set send buf size
	if( 0 == ret )
	{
		int snd_size = 1024*1024;    
		socklen_t optlen = sizeof(snd_size); 
		int err = setsockopt(listenFd, SOL_SOCKET, SO_SNDBUF,(char*) &snd_size, optlen); 
		if(err<0){ 
			printf("set send buf size error\n"); 
			ret=-1;
		} 
	}
	if( 0 == ret )
	{
		//set recv buf size
		int rcv_size = 1024*1024;    
		socklen_t optlen = sizeof(rcv_size); 
		int err = setsockopt(listenFd,SOL_SOCKET,SO_RCVBUF, (char *)&rcv_size, optlen); 
		if(err<0){ 
			printf("set recv buf size error\n"); 
			ret=-1;
		} 
	}
	if( 0 == ret ) {
		if( bind( listenFd, (struct sockaddr*)&addr, sizeof( addr ) ) < 0 ) {
			sp_syslog( LOG_WARNING, "bind failed, errno %d, %s", errno, strerror( errno ) );
			ret = -1;
		}
	}

	if( 0 == ret ) {
		if( ::listen( listenFd, 1024 ) < 0 ) {
			sp_syslog( LOG_WARNING, "listen failed, errno %d, %s", errno, strerror( errno ) );
			ret = -1;
		}
	}

	if( 0 != ret && listenFd >= 0 ) sp_close( listenFd );

	if( 0 == ret ) {
		* fd = listenFd;

		if( mode > 0 ) {
			if( 0 != fchmod( *fd, (mode_t)mode ) ) {
				sp_syslog( LOG_WARNING, "fchmod fail, errno %d, %s", errno, strerror( errno ) );
			}
		}

		sp_syslog( LOG_NOTICE, "Listen on [%s]", path );
	}

#endif

	return ret;
}

int IOUtils::initDaemon(const char * workdir)
{
#ifndef WIN32

	int		i;
	pid_t	pid;

	if ( (pid = fork()) < 0)
		return (-1);
	else if (pid)
		_exit(0);			/* parent terminates */

	/* child 1 continues... */

	if (setsid() < 0)			/* become session leader */
		return (-1);

	assert( signal( SIGHUP,  SIG_IGN ) != SIG_ERR );
	assert( signal( SIGPIPE, SIG_IGN ) != SIG_ERR );
	assert( signal( SIGALRM, SIG_IGN ) != SIG_ERR );
	assert( signal( SIGCHLD, SIG_IGN ) != SIG_ERR );

	if ( (pid = fork()) < 0)
		return (-1);
	else if (pid)
		_exit(0);			/* child 1 terminates */

	/* child 2 continues... */

	if( NULL != workdir ) chdir( workdir );		/* change working directory */

	/* close off file descriptors */
	for (i = 0; i < 64; i++)
		close(i);

	/* redirect stdin, stdout, and stderr to /dev/null */
	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);

#endif

	return (0);				/* success */
}

