
#include "msghandler.h"
#include <time.h>
#include "../inc/logger.h"
#include "../client/PublishData.h"
#include "../client/AppServerImpl.h"
#include "GeneratedPublish.h"
#include "md5.h"
#include <map>
#include <set>
#include <iostream>
#include <unordered_map>
#include <unordered_set>

#ifdef _WIN32
#include <shlwapi.h>
#endif
int MsgHandler :: mMsgSeq = 0;

//#define TONGJI

MsgHandler :: MsgHandler( ServiceManager * manager )
{
	hasSuperRights=false;
	stoped=false;
	memset( &mSid, 0, sizeof( mSid ) );
	mServiceManager = manager;
}

MsgHandler :: ~MsgHandler()
{
	//LOG4_ERROR("~MsgHandler");
}

void MsgHandler::post(SP_Response * response, const char * buffer, size_t size, AppAddress toSid)
{
	if(mSid==toSid)
		return;
	SP_Message * msg = new SP_Message();
	msg->setCompletionKey( ++mMsgSeq );
	msg->getMsg()->append( buffer ,size);
	msg->getToList()->push_back(SP_Sid_t(toSid));
	response->addMessage( msg );
}
void MsgHandler::post(SP_Response * response, const char * buffer, size_t size, unordered_set<unsigned int>* setIds)
{
	if(setIds->size()==0)
		return;
	SP_Message * msg = new SP_Message();
	msg->setCompletionKey( ++mMsgSeq );
	msg->getMsg()->append( buffer ,size);
	msg->getToList()->reserve(setIds->size());
	std::vector<unsigned int> vecAddr;
	unordered_set<unsigned int>::const_iterator it = setIds->begin();
	while(it!=setIds->end())
	{
		if(*it!=mSid)
		    vecAddr.push_back(*it);
		it++;
	}
	bool firstpub=true;
	std::vector<int> vecType;
	mServiceManager->getAppType(vecAddr,vecType);
	for(int i=0;i<vecAddr.size();i++)
	{
		if(mInfo.type()==3)
		{
			if(vecType.at(i)!=3)
				msg->getToList()->push_back(SP_Sid_t(vecAddr.at(i)));
		}
		else
		{
			if(vecType.at(i)==3)
			{
				if(firstpub)
				{
					msg->getToList()->push_back(SP_Sid_t(vecAddr.at(i)));
					firstpub=false;
				}
			}
			else
				msg->getToList()->push_back(SP_Sid_t(vecAddr.at(i)));
		}
	}
	while(it!=setIds->end())
	{
		if(mInfo.type()==3)
		{
			MsgExpress::AppInfo info;
			if(!mServiceManager->getAppInfo(*it,info))
			{
				it++;
				continue;
			}
			if(info.logininfo().type()==3 && mInfo.type()==3 && info.logininfo().group()==mInfo.group())
			{
				it++;
				continue;
			}
		}
	    msg->getToList()->push_back(SP_Sid_t(*it));
		it++;
	}
	response->addMessage( msg );
}
void MsgHandler::broadcast(SP_Response * response, const char * buffer, size_t size, AppAddress * ignoreSid)
{
	
}
void MsgHandler::broadcast(SP_Response * response, const string& msg, AppAddress * ignoreSid)
{
	broadcast(response,msg.c_str(),msg.size(),ignoreSid);
}

void MsgHandler :: reply(Package* package,const Message& replyMsg,SP_Response * response)
{
	std::string msg;
	PackageHeader header;
	header.command=package->getCommand();
	header.protocol=package->getHeader().protocol;
	header.type=PackageHeader::Response;
	header.serialnum=package->getSerialNum();
	header.srcaddr=package->getDstAddr();
	header.dstaddr=package->getSrcAddr();
	if(MessageUtil::serializePackageToString(header,replyMsg,true,30,&msg))
	{
		response->getReply()->getMsg()->append(msg.c_str(),msg.size());
		response->getReply()->setCompletionKey( ++mMsgSeq );
	}
	else
		LOG4_ERROR("MsgHandler :: reply,serialze error");
}
void MsgHandler :: reply(Package* package,unsigned int errCode,string errMsg,SP_Response * response)
{
	MsgExpress::ErrMessage resp;
	resp.set_errcode(errCode);
	resp.set_errmsg(errMsg);
	std::string msg;
	PackageHeader header;
	header.command=0;
	header.protocol=package->getHeader().protocol;
	header.type=PackageHeader::Response;
	header.serialnum=package->getSerialNum();
	header.srcaddr=package->getDstAddr();
	header.dstaddr=package->getSrcAddr();
	if(header.protocol==0)
	{
		if(MessageUtil::serializePackageToString(header,resp,true,30,&msg))
		{
			response->getReply()->getMsg()->append(msg.c_str(),msg.size());
			response->getReply()->setCompletionKey( ++mMsgSeq );
		}
		else
			LOG4_ERROR("MsgHandler :: reply,serialze error");
	}
	else
	{
		header.bodysize=sizeof(errCode);
		int size=header.bodysize+sizeof(PackageHeader);
		char* buf=new char[size];
		header.write(buf);
		*(unsigned int*)(buf+sizeof(PackageHeader))=htonl(errCode);
		response->getReply()->getMsg()->append(buf,size);
		response->getReply()->setCompletionKey( ++mMsgSeq );
		delete []buf;
	}
}
string formatNumber(unsigned int value)
{
	char ch[36];
	sprintf(ch,"%d",value);
	return string(ch);
}

void MsgHandler :: subscribe(Package* package, SP_Response * response)
{
	if(!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package,resp,response);
		return;
	}
	if(package->getClassType().compare(MsgExpress::SubscribeData::default_instance().GetTypeName())==0)
	{
		MsgExpress::SubscribeData* sub=(MsgExpress::SubscribeData*)package->getMessage().get();
		if(sub==NULL)
			return;
		unsigned int addr=mSid;
		if(sub->useraddr()!=0)
		{
			addr=sub->useraddr();
			LOG4_INFO("Subscribe for %d[0x%x],topic:0X%x,subid:%d,content:%s",addr,addr,sub->topic(),sub->subid(),sub->ShortDebugString().c_str());
		}
		else
			LOG4_INFO("Subscribe for myself %d[0x%x],topic:0X%x,subid:%d,content:%s",addr,addr,sub->topic(),sub->subid(),sub->ShortDebugString().c_str());
		mServiceManager->subscribe(*sub,addr);
	}
	else if(package->getClassType().compare(MsgExpress::SimpleSubscription::default_instance().GetTypeName())==0)
	{
		MsgExpress::SimpleSubscription* sub=(MsgExpress::SimpleSubscription*)package->getMessage().get();
		if(sub==NULL)
			return;
		unsigned int addr=mSid;
		if(sub->useraddr()!=0)
		{
			addr=sub->useraddr();
			LOG4_INFO("Subscribe for %d[0x%x],topic:0X%x,subid:%d,content:%s",addr,addr,sub->topic(),sub->subid(),sub->ShortDebugString().c_str());
		}
		else
			LOG4_INFO("Subscribe for myself %d[0x%x],topic:0X%x,subid:%d,content:%s",addr,addr,sub->topic(),sub->subid(),sub->ShortDebugString().c_str());
		mServiceManager->subscribe(*sub,addr);
	}
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	resp.set_msg("subscribe succeed.");
	reply(package,resp,response);
}
void MsgHandler :: complexSubscribe(Package* package, SP_Response * response)
{
	if(!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package,resp,response);
		return;
	}
	MsgExpress::ComplexSubscribeData* complexSub=(MsgExpress::ComplexSubscribeData*)package->getMessage().get();
	if(complexSub==NULL)
		return;
	LOG4_INFO("ComplexSubscribe:%s\r",complexSub->ShortDebugString().c_str());

	for(int i=0;i<complexSub->unsub_size();i++)
	{
		unsigned int addr=mSid;
		if(complexSub->unsub(i).useraddr()!=0)
		    addr=complexSub->unsub(i).useraddr();
		mServiceManager->unSubscribe(complexSub->unsub(i).subid(),addr);
	}
	for(int i=0;i<complexSub->sub_size();i++)
	{
		unsigned int addr=mSid;
		if(complexSub->sub(i).useraddr()!=0)
		    addr=complexSub->sub(i).useraddr();
		mServiceManager->subscribe(complexSub->sub(i),addr);
	}
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package,resp,response);
}
void MsgHandler :: unsubscribe(Package* package, SP_Response * response)
{
	if(!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package,resp,response);
		return;
	}
	MsgExpress::UnSubscribeData* unsub=(MsgExpress::UnSubscribeData*)package->getMessage().get();
	if(unsub==NULL)
		return;
	LOG4_INFO("unSubscribe:%s\r",unsub->ShortDebugString().c_str());
	int subId=unsub->subid();
	unsigned int addr=mSid;
	if(unsub->useraddr()!=0)
		addr=unsub->useraddr();
	mServiceManager->unSubscribe(subId,addr);

	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package,resp,response);
}
void MsgHandler :: regCallback(const CallbackArg& arg)
{
}
void MsgHandler :: login( SP_Request * request,Package* package, SP_Response * response)
{
	MsgExpress::LoginInfo* login=(MsgExpress::LoginInfo*)package->getMessage().get();
	if(login==NULL)
		return;
	login->set_ip(request->getClientIP());
	mInfo.CopyFrom(*login);
	mServiceManager->regApp(mSid,*login);
	time_t tm;
	time(&tm);
	uint64_t brokertime=getCurrentTime();
	MsgExpress::LoginResponse resp;
	resp.set_addr(mSid);
	resp.set_brokertime(brokertime);
	reply(package,resp,response);
	
	vector<int> vecApps;
	if(login->type()>0)
	{
		string md5code=md5(login->auth());
		if(md5code==mServiceManager->getAuthData())
			hasSuperRights=true;

		int total=login->serviceid_size();
		if(hasSuperRights)
		{
			for(int i=0;i<total;i++)
			{
				int serviceId=login->serviceid(i);
				if(mServiceList.count(serviceId)==0)
				{
					mServiceList.insert(serviceId);
					vecApps.push_back(serviceId);
				}
			}
			mServiceManager->regService(vecApps,mSid);
		}
	}
	
	MsgExpress::PublishData pub;
	pub.set_topic(MsgExpress::TOPIC_LOGIN);
	PublishHelperPtr helper=CreatePublishHelper(&pub);
	helper->AddString(MsgExpress::KEY_BROKER,mServiceManager->getBrokerName());
	helper->AddUInt32(MsgExpress::KEY_ADDR,mSid);
	helper->AddUInt32(MsgExpress::KEY_TYPE,login->type());
	helper->AddUInt32(MsgExpress::KEY_GROUP,login->group());
	helper->AddString(MsgExpress::KEY_UUID,login->uuid());
	helper->AddString(MsgExpress::KEY_AUTH,login->auth());
	helper->AddString(MsgExpress::KEY_NAME,login->name());
	helper->AddString(MsgExpress::KEY_IP,login->ip());
	helper->AddDatetime(MsgExpress::KEY_STARTTIME,login->starttime());
	helper->AddDatetime(MsgExpress::KEY_LOGINTIME,tm);
	helper->AddDatetime(MsgExpress::KEY_TIME,tm);
	for(unsigned int i=0;i<vecApps.size();i++)
		helper->AddInt32(MsgExpress::KEY_SERVICE,vecApps.at(i));

	publish(0,0,0,&pub,response);
	LOG4_INFO("AppServer login:addr=%s,%s,resp:%s", mSid.toString().c_str(), login->ShortDebugString().c_str(), resp.ShortDebugString().c_str());
}
void MsgHandler :: logout( SP_Request * request,Package* package, SP_Response * response)
{
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package,resp,response);

	LOG4_INFO("AppServer logout:addr=%s,%s", mSid.toString().c_str(), package->getMessage()->ShortDebugString().c_str());
}
void MsgHandler :: heartBeat( SP_Request * request,Package* package, SP_Response * response)
{
	MsgExpress::HeartBeat* heartBeat=(MsgExpress::HeartBeat*)package->getMessage().get();
	if(heartBeat==NULL)
		return;
	uint64_t brokertime=getCurrentTime();

	MsgExpress::HeartBeatResponse resp;
	resp.set_retcode(0);
	resp.set_brokertime(brokertime);
	resp.set_servertime(heartBeat->servertime());
	reply(package,resp,response);
	LOG4_DEBUG("heartBeat Response");
	time_t tm;
	time(&tm);

	MsgExpress::PublishData pub;
	pub.set_topic(MsgExpress::TOPIC_HEARTBEAT);
	PublishHelperPtr helper=CreatePublishHelper(&pub);
	helper->AddDatetime(MsgExpress::KEY_HBTIME,tm);
	helper->AddString(MsgExpress::KEY_BROKER,mServiceManager->getBrokerName());
	helper->AddUInt32(MsgExpress::KEY_ADDR,mSid);
	helper->AddString(MsgExpress::KEY_UUID,mInfo.uuid());
	helper->AddUInt32(MsgExpress::KEY_CPU,heartBeat->cpu());
	helper->AddUInt32(MsgExpress::KEY_TOPMEM,heartBeat->topmemory());
	helper->AddUInt32(MsgExpress::KEY_MEM,heartBeat->memory());
	helper->AddUInt32(MsgExpress::KEY_CSQUEUE,heartBeat->sendqueue());
	helper->AddUInt32(MsgExpress::KEY_CRQUEUE,heartBeat->receivequeue());

	helper->AddUInt64(MsgExpress::KEY_RECVREQUEST,heartBeat->recvrequest());
	helper->AddUInt64(MsgExpress::KEY_SENTREQUEST,heartBeat->sendrequest());
	helper->AddUInt64(MsgExpress::KEY_RECVRESPONSE,heartBeat->recvresponse());
	helper->AddUInt64(MsgExpress::KEY_SENTRESPONSE,heartBeat->sendresponse());
	helper->AddUInt64(MsgExpress::KEY_RECVPUBLISH,heartBeat->recvpub());
	helper->AddUInt64(MsgExpress::KEY_SENTPUBLISH,heartBeat->sendpub());

	//CountInfo info;
	//if(mServiceManager->getCountInfo(mSid.value,info))
	//{
	//	helper->AddUInt64(MsgExpress::KEY_QUEUELENGTH,info.queueLength);

	//	helper->AddUInt64(MsgExpress::KEY_RECVREQUESTB,info.inRequestB);
	//	helper->AddUInt64(MsgExpress::KEY_SENTREQUESTB,info.outRequestB);
	//	helper->AddUInt64(MsgExpress::KEY_RECVRESPONSEB,info.inResponseB);
	//	helper->AddUInt64(MsgExpress::KEY_SENTRESPONSEB,info.outResponseB);
	//	helper->AddUInt64(MsgExpress::KEY_RECVPUBLISHB,info.inPublishB);
	//	helper->AddUInt64(MsgExpress::KEY_SENTPUBLISHB,info.outPublishB);
	//}
	publish(0,0,0,&pub,response);
}
#define COMPARE(left,right,op,result) \
{ \
	if(op==MsgExpress::Bigger) \
	    result=( left>right); \
	else if(op==MsgExpress::BiggerOrEqual) \
	    result=( left>=right); \
	else if(op==MsgExpress::Less) \
	    result=( left<right); \
	else if(op==MsgExpress::LessOrEqual) \
	    result=( left<=right); \
	else if(op==MsgExpress::NotEqual) \
	    result=( left!=right); \
} 
bool Satisfy(const google::protobuf::RepeatedPtrField<std::string>& value,const MsgExpress::ConditionValue& conValue,MsgExpress::DataType type)
{
	bool ok=false;
	for(int i=0;i<value.size();i++)
	{
		if(type==MsgExpress::INT32)
		{
			int left=(int)ntohl(*(int*)value.Get(i).data());
			int right=(int)ntohl(*(int*)conValue.value().data());
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::UINT32)
		{
			unsigned int left=(unsigned int)ntohl(*(unsigned int*)value.Get(i).data());
			unsigned int right=(unsigned int)ntohl(*(unsigned int*)conValue.value().data());
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::INT64)
		{
			int64_t left=(int64_t)ntohll(*(int64_t*)value.Get(i).data());
			int64_t right=(int64_t)ntohll(*(int64_t*)conValue.value().data());
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::UINT64)
		{
			uint64_t left=(uint64_t)ntohll(*(uint64_t*)value.Get(i).data());
			uint64_t right=(uint64_t)ntohll(*(uint64_t*)conValue.value().data());
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::FLOAT)
		{
			float left=*(float*)value.Get(i).data();
			float right=*(float*)conValue.value().data();
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::DOUBLE)
		{
			double left=*(double*)value.Get(i).data();
			double right=*(double*)conValue.value().data();
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::DATETIME)
		{
			time_t left=(time_t)ntohll(*(time_t*)value.Get(i).data());
			time_t right=(time_t)ntohll(*(time_t*)conValue.value().data());
			COMPARE(left,right,conValue.operator_(),ok);
		}
		else if(type==MsgExpress::STRING)
		{
			COMPARE(value.Get(i),conValue.value(),conValue.operator_(),ok);
		}
		else if(type==MsgExpress::BINARY)
		{
			COMPARE(value.Get(i),conValue.value(),conValue.operator_(),ok);
		}
		if(ok)
			break;
	}
	return ok;
}

void MsgHandler :: publish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,MsgExpress::PublishData* pubData, SP_Response * response)
{
	LOG4_DEBUG("Publish:topic=0X%x,",pubData->topic());
	map<int,MsgExpress::DataItem> mapKeyValue;
	for(int i=0;i<pubData->item_size();i++)
	{
		mapKeyValue[pubData->item(i).key()].CopyFrom(pubData->item(i));
	}
	map<int,vector<MsgExpress::SubscribeData*>*> mapUserList;
	mServiceManager->getSubscriberList(pubData,mapUserList);
	map<int,vector<MsgExpress::SubscribeData*>*>::iterator it=mapUserList.begin();
	while(it!=mapUserList.end())
	{
		LOG4_DEBUG("Publish check user:%d",it->first);
		if(mSid.Id!=(unsigned int)it->first)
		{
			MsgExpress::PublishData copyData;//(*pubData);
			copyData.set_topic(pubData->topic());
			std::set<int> setIndexes;
			vector<MsgExpress::SubscribeData*>::iterator iter=it->second->begin();
			while(iter!=it->second->end())
			{
				bool allOK=true;
				for(int i=0;i<(*iter)->excondition_size();i++)
				{
					int key=(*iter)->excondition(i).key();
					MsgExpress::DataType type=(*iter)->excondition(i).type();
					bool oneOK=false;
					for(int j=0;j<(*iter)->excondition(i).value_size();j++)
					{
						const MsgExpress::ConditionValue& conValue=(*iter)->excondition(i).value(j);
						oneOK=Satisfy(mapKeyValue[key].value(),conValue,type);
						if(oneOK)
							break;
					}
					if(!oneOK)
					{
						allOK=false;
						break;
					}
				}
				if(allOK)
				{
					if((*iter)->index_size()>0)
					{
						for(int i=0;i<(*iter)->index_size();i++)
						{
							int key=(*iter)->index(i);
							if(setIndexes.find(key)!=setIndexes.end())
								continue;
							if(mapKeyValue.find(key)!=mapKeyValue.end())
							{
								MsgExpress::DataItem *item=copyData.add_item();
								item->CopyFrom(mapKeyValue[key]);
								setIndexes.insert(key);
							}
						}
					}
					else
					{
						for(int j=0;j<pubData->item_size();j++)
						{
							int key=pubData->item(j).key();
							if(setIndexes.find(key)!=setIndexes.end())
								continue;
							if(mapKeyValue.find(key)!=mapKeyValue.end())
							{
								MsgExpress::DataItem *item=copyData.add_item();
								item->CopyFrom(pubData->item(j));
								setIndexes.insert(key);
							}
						}
					}
			        copyData.add_subid((*iter)->subid());
				}
				delete *iter;
				
				iter++;
			}

			if(copyData.subid_size()>0)
			{
				string data;
				PackageHeader header;
				header.command=cmd;
				header.type=PackageHeader::Publish;
				header.serialnum=serial;
				header.srcaddr=srcAddr;
				header.dstaddr=0;
				MessageUtil::serializePackageToString(header,copyData,true,50,&data);
				post(response,data.data(),data.size(),(it->first));
				/*CountInfo countInfo;
				countInfo.inPublish=1;
				countInfo.inPublishB=data.size();
				mServiceManager->updateCountInfo(it->first,countInfo);*/
				LOG4_DEBUG("Publish to %d,topic=%d,subid=%d",it->first,pubData->topic(),copyData.subid(0));
			}
		}
		delete it->second;
		it++;
	}
}

void MsgHandler :: complexPublish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response)
{
	int complexKey;//只支持一个item是Complex
	map<uint64_t, const MsgExpress::Complex*> mapCompList;
	MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)package->getMessage().get();
	MsgExpress::PublishData baseData(*pubData);
	baseData.clear_item();
	const RepeatedPtrField<MsgExpress::DataItem>& list=pubData->item();
	RepeatedPtrField<MsgExpress::DataItem>::const_iterator iter=list.begin();
	while(iter!=list.end())
	{
		if(iter->type()!=MsgExpress::COMPLEX)
		{
			MsgExpress::DataItem* item=baseData.add_item();
			item->CopyFrom(*iter);
		}
		if(iter->type()==MsgExpress::COMPLEX && iter->compval_size()>0)
		{
			complexKey=iter->key();
			for(int i=0;i<iter->compval_size();i++)
			{
				mapCompList[iter->compval(i).a()]=&(iter->compval(i));
			}
		}
		iter++;
	}
	bool first=true;
	map<int,vector<MsgExpress::SubscribeData*>*> mapUserList;
	mServiceManager->getSubscriberList(pubData,mapUserList);
	map<int,vector<MsgExpress::SubscribeData*>*>::iterator it=mapUserList.begin();
	while(it!=mapUserList.end())
	{
		int complexKey;
		const RepeatedPtrField<MsgExpress::Complex>* valueList=NULL;
		bool found=false;
		vector<MsgExpress::SubscribeData*>::iterator iter2=it->second->begin();
		while(iter2!=it->second->end())
		{
			if(found)
				break;
			for(int i=0;i<(*iter2)->condition_size();i++)
			{
				if((*iter2)->condition(i).type()==MsgExpress::COMPLEX && (*iter2)->condition(i).compval_size()>0)
				{
					complexKey=(*iter2)->condition(i).key();
					valueList=&(*iter2)->condition(i).compval();
					found=true;
					break;
				}
			}
			iter2++;
		}
		if(found)
		{
			MsgExpress::PublishData copyData(baseData);
			for(RepeatedPtrField<MsgExpress::Complex>::const_iterator iter=valueList->begin();iter!=valueList->end();iter++)
			{
				int64_t key=iter->a();
				if(mapCompList.find(key)!=mapCompList.end())
				{
					MsgExpress::DataItem* item=copyData.add_item();
					item->set_key(complexKey);
					item->set_type(MsgExpress::BINARY);
					item->add_rawval(mapCompList[key]->b());
				}
			}
			string data;
			PackageHeader header;
			header.command=cmd;
			header.type=PackageHeader::Publish;
			header.serialnum=serial;
			header.srcaddr=srcAddr;
			header.dstaddr=0;
			MessageUtil::serializePackageToString(header,copyData,true,50,&data);
			post(response,data.data(),data.size(),(it->first));
		}
		else
		{
			MsgExpress::AppInfo info;
			mServiceManager->getAppInfo(it->first,info);
			if(mInfo.type()==3 && info.logininfo().type()!=3)
			    post(response,package->getContent(),package->getSize(),(it->first));
			else if(mInfo.type()!=3 && info.logininfo().type()!=3)
			{
				post(response,package->getContent(),package->getSize(),(it->first));
			}
			else if(first && mInfo.type()!=3 && info.logininfo().type()==3)
			{
				first=false;
				post(response,package->getContent(),package->getSize(),(it->first));
			}
		}
		it++;
	}
}

void MsgHandler :: simplePublish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response)
{
	unordered_set<unsigned int>* setIds = NULL;
	int topic=0;
	//uint64_t t1=getCurrentTime();
	if(package->getClassType().compare(MsgExpress::PublishData::default_instance().GetTypeName())==0)
	{
		MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)package->getMessage().get();
		//LOG4_INFO("Publish:topic:%d,from:%d[0X%x],%s",pubData->topic(),srcAddr,srcAddr,pubData->ShortDebugString().c_str());
		topic=pubData->topic();
		setIds=mServiceManager->getSubscriberIdList(pubData);
	}
	else if(package->getHeader().pubmsg)
	{
		topic=package->getCommand();
		string pubData;
		if(package->getHeader().iszip)
		{
		    bool unzipOK = MessageUtil::unCompress(package->getBodyData(),package->getBodySize(),package->getHeader().compratio,pubData);
		    if(!unzipOK)
			{
				LOG4_ERROR("Publish failed,unzip failed,topic:%s",package->getClassType().c_str());
				return;
			}
		}
		else
		    pubData.assign(package->getBodyData(),package->getBodySize());
		setIds=mServiceManager->getSubscriberIdList(topic,pubData);
	}
	//uint64_t t2=getCurrentTime();
	if(setIds->find(mSid.Id)!=setIds->end())
		setIds->erase(setIds->find(mSid.Id));
	//uint64_t t3=getCurrentTime();
	post(response,package->getContent(),package->getSize(),setIds);
	/*uint64_t t4=getCurrentTime();
	int off=t4-t1;
	if(off>5)
	{
		int off1=t2-t1;
		int off2=t3-t2;
		int off3=t4-t3;
		LOG4_WARN("Publish cost time:%d(%d,%d,%d),%s",off,off1,off2,off3,pubData->ShortDebugString().c_str());
	}*/
	string tmp;
	char buf[32];
	unordered_set<unsigned int>::const_iterator it = setIds->begin();
	while(it!=setIds->end())
	{
		//CountInfo countInfo;
		//countInfo.inPublish=1;
		//countInfo.inPublishB=package->getSize();
		//mServiceManager->updateCountInfo(*it,countInfo);
		sprintf(buf,"%d,",*it);
		tmp.append(buf);
	    it++;
	}
	LOG4_DEBUG("Publish:topic:%d,from:%s,to:%s,serial:%d,content:", topic, mSid.toString().c_str(), tmp.c_str(), serial);
}
void MsgHandler :: publish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response)
{
	if(package->getClassType().compare(MsgExpress::PublishData::default_instance().GetTypeName())==0)
	{
		MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)package->getMessage().get();
		bool complex=false;
		const RepeatedPtrField<MsgExpress::DataItem>& list=pubData->item();
		RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=list.begin();
		while(it!=list.end())
		{
			if(it->type()==MsgExpress::COMPLEX && it->compval_size()>0)
			{
				complex=true;
				break;
			}
			it++;
		}
		if(!complex)
			simplePublish( cmd, serial,srcAddr,package,  response);
		else
			complexPublish( cmd, serial,srcAddr,package,  response);
	}
	else if(package->getHeader().pubmsg)
	{
		simplePublish( cmd, serial,srcAddr,package,  response);
	}
}
//void MsgHandler :: simplePublish2(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response)
//{
//	unordered_set<int>* setIds=mServiceManager->getSubscriberIdList(package->getHeader().topic);
//	post(response,package->getContent(),package->getSize(),setIds);
//#ifdef TONGJI
//	unordered_set<int>::const_iterator it=setIds->begin();
//	while(it!=setIds->end())
//	{
//		CountInfo countInfo;
//		countInfo.inPublish=1;
//		countInfo.inPublishB=package->getSize();
//		mServiceManager->updateCountInfo(*it,countInfo);
//	    it++;
//	}
//#endif
//}
//void MsgHandler :: simplePublish3(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response)
//{
//	MsgExpress::PublishData* pubData=new MsgExpress::PublishData();
//	pubData->set_topic(package->getHeader().topic);
//	unordered_set<int>* setIds=mServiceManager->getSubscriberIdList(pubData);
//	post(response,package->getContent(),package->getSize(),setIds);
//#ifdef TONGJI
//	unordered_set<int>::const_iterator it=setIds->begin();
//	while(it!=setIds->end())
//	{
//		CountInfo countInfo;
//		countInfo.inPublish=1;
//		countInfo.inPublishB=package->getSize();
//		mServiceManager->updateCountInfo(*it,countInfo);
//	    it++;
//	}
//#endif
//}
void MsgHandler :: createprocess(Package* package, SP_Response * response)
{
#ifdef _WIN32
	MsgExpress::StartupApplication* info=(MsgExpress::StartupApplication*)package->getMessage().get();
	BOOL ret=FALSE;
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	resp.set_msg("");

	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);

	char oldDir[1024], newDir[1024];
	GetCurrentDirectoryA(1024,oldDir);

	string args="";
	for(int i=0;i<info->args_size();i++)
	{
		args+=info->args(i);
		args+=" ";
	}
	strcpy(newDir, info->exefullname().c_str());
	::PathRemoveFileSpecA(newDir);
	if(SetCurrentDirectoryA(newDir))
	{
	    LOG4_INFO("CreateProcess:%s",info->exefullname().c_str());
		char buf[1024]={0};
		strcpy_s(buf,args.c_str());
		ret=CreateProcessA(info->exefullname().c_str(),buf, NULL, NULL, FALSE, DETACHED_PROCESS | CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
		if(ret)
		{
	        LOG4_INFO("CreateProcess OK");
		}
		else
		{
			 int error = GetLastError();  
			 LOG4_ERROR("CreateProcess failed,error:%d",error);
			 resp.set_retcode(error);
			 resp.set_msg("Failed");
		}
	    SetCurrentDirectoryA(oldDir);
	}
	else
	{
		LOG4_ERROR("Cant startup application,SetCurrentDirectoryA failed,path=%s",newDir);
	}
	reply(package,resp,response);
#endif
}
void MsgHandler :: process( SP_Request * request,Package* package, SP_Response * response)
{
	package->setSrcAddr(mSid);
	PackageHeader::MsgType packagetype=package->getPackageType();
	LOG4_DEBUG("Process package:%s",package->getDebugString().c_str());
#ifdef TONGJI
	{
		CountInfo countInfo;
		if(packagetype==PackageHeader::Request && package->getApp()!=MsgExpress::APPID)
		{
			countInfo.outRequest=1;
			countInfo.outRequestB=package->getSize();
		}
		else if(packagetype==PackageHeader::Response)
		{
			countInfo.outResponse=1;
			countInfo.outResponseB=package->getSize();
		}
		else if(packagetype==PackageHeader::Publish)
		{
			countInfo.outPublish=1;
			countInfo.outPublishB=package->getSize();
		}
		mServiceManager->updateCountInfo(mSid.value,countInfo);
	}
#endif
	if(packagetype==PackageHeader::Request)
	{
		unsigned int app=package->getApp();
		if(app==MsgExpress::APPID)
		{
			if(package->getHeader().protocol!=0)
			{
				LOG4_ERROR(package->getDebugString().c_str());
				return ;
			}
			std::string msgtype=CommandConfig::getInstance().getClazz(package->getHeader().command,(PackageHeader::MsgType)package->getHeader().type,package->getHeader().pubmsg);
			package->setClassType(msgtype);
			if(!MessageUtil::parse(package))
			{
				LOG4_ERROR("Request to broker,but parse message failed! content:%s",package->getDebugString().c_str());
				return;
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::SubscribeData::default_instance().GetTypeName())
			{
				subscribe(package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::UnSubscribeData::default_instance().GetTypeName())
			{
				unsubscribe(package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::ComplexSubscribeData::default_instance().GetTypeName())
			{
				complexSubscribe(package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::SimpleSubscription::default_instance().GetTypeName())
			{
				subscribe(package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::LoginInfo::default_instance().GetTypeName())
			{
				login(request,package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::Logout::default_instance().GetTypeName())
			{
				logout(request,package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::HeartBeat::default_instance().GetTypeName())
			{
				heartBeat(request,package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::StartupApplication::default_instance().GetTypeName())
			{
				createprocess(package,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::StopBroker::default_instance().GetTypeName())
			{
				if(hasSuperRights)
				{
					LOG4_WARN("Broker will restart");
					sp_sleep(1000);
					exit(0);//应急办法
				}
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::RestartApp::default_instance().GetTypeName())
			{
				if(hasSuperRights)
				{
					MsgExpress::CommonResponse resp;
					MsgExpress::RestartApp* req=(MsgExpress::RestartApp*)package->getMessage().get();
					unsigned int addr=mServiceManager->getAddress(req->uuid());
					if(addr>0){
						LOG4_WARN("Server will restart,addr=%d,uuid=%s",addr,req->uuid().c_str());
						AppAddress sid(addr);
			            post(response,package->getContent(),package->getSize(),sid);
						resp.set_retcode(0);					
					    resp.set_msg("restart app ok.");
					}
					else
					{
						LOG4_WARN("Not find app,restart failed,uuid=%s",req->uuid().c_str());
					    resp.set_retcode(-1);
						resp.set_msg("Not find app.");
					}
					reply(package,resp,response);
				}
				else
				{
					MsgExpress::CommonResponse resp;
					resp.set_retcode(1);
					resp.set_msg("Authenrity is not correct");
					reply(package,resp,response);
				}
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::GetAppList::default_instance().GetTypeName())
			{
				MsgExpress::AppList resp;
				mServiceManager->getAppInfo(*(MsgExpress::GetAppList*)package->getMessage().get(),resp);
				reply(package,resp,response);
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::GetAppInfo::default_instance().GetTypeName())
			{
				MsgExpress::GetAppInfo* req=(MsgExpress::GetAppInfo*)package->getMessage().get();
				unsigned int addr=req->addr();
				if(hasSuperRights)
				{
					MsgExpress::AppInfo resp;
					if(mServiceManager->getAppInfo(addr,resp))
					    reply(package,resp,response);
					else 
					{
						reply(package,Gateway::ERRCODE_NOAPPINFO,Gateway::ERRMSG_NOAPPINFO,response);
						LOG4_ERROR("get app info failed,addr=%d",addr);
					}
				}
				else
				{
					MsgExpress::CommonResponse resp;
					resp.set_retcode(1);
					resp.set_msg("Authenrity is not correct");
					reply(package,resp,response);
				}
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::UpdateAppStatus::default_instance().GetTypeName())
			{
				MsgExpress::UpdateAppStatus* appStatus=(MsgExpress::UpdateAppStatus*)package->getMessage().get();
				if(hasSuperRights)
				{
					mServiceManager->updateAppStatus(*appStatus);
					MsgExpress::CommonResponse resp;
					resp.set_retcode(0);
					reply(package,resp,response);
				}
				else
				{
					MsgExpress::CommonResponse resp;
					resp.set_retcode(1);
					resp.set_msg("Authenrity is not correct");
					reply(package,resp,response);
				}
			}
			else if(package->getMessage()->GetTypeName()==MsgExpress::KickOffApp::default_instance().GetTypeName())
			{
				MsgExpress::KickOffApp* kick=(MsgExpress::KickOffApp*)package->getMessage().get();
				if(hasSuperRights)
				{
					AppAddress sid(kick->addr());
			        post(response,package->getContent(),package->getSize(),sid);

				    MsgExpress::CommonResponse resp;
				    resp.set_retcode(0);
				    reply(package,resp,response);
				}
				else
				{
					MsgExpress::CommonResponse resp;
					resp.set_retcode(1);
					resp.set_msg("Authenrity is not correct");
					reply(package,resp,response);
				}
			}
			else {
				LOG4_ERROR("error message,content:%s",package->getDebugString().c_str());
				return;
			}
		}
		else if(package->isMulticast())
		{
			std::vector<AppAddress> sidlist;
			std::vector<AppAddress> sidlistproxy;
			mServiceManager->getServiceProviderList(package->getApp(),sidlist,sidlistproxy);
			for(int i=0;i<sidlist.size();i++)
			{
				if(sidlist.at(i)==mSid)
					continue;
				post(response,package->getContent(),package->getSize(),sidlist.at(i));
#ifdef TONGJI
			CountInfo countInfo;
			countInfo.inRequest=1;
			countInfo.inRequestB=package->getSize();
			mServiceManager->updateCountInfo(sidlist.get(i).value,countInfo);
#endif
			}
			for(int i=0;i<sidlistproxy.size();i++)
			{
				if(sidlistproxy.at(i)==mSid)
					continue;
				if(mInfo.type()==3)
				    break;
				post(response,package->getContent(),package->getSize(),sidlistproxy.at(i));
				break;
#ifdef TONGJI
			CountInfo countInfo;
			countInfo.inRequest=1;
			countInfo.inRequestB=package->getSize();
			mServiceManager->updateCountInfo(sidlist.get(i).value,countInfo);
#endif
			}
		}
		else if(package->getDstAddr()!=0)
		{
			AppAddress sid(package->getDstAddr());
			post(response,package->getContent(),package->getSize(),sid);
#ifdef TONGJI
			CountInfo countInfo;
			countInfo.inRequest=1;
			countInfo.inRequestB=package->getSize();
			mServiceManager->updateCountInfo(sid.value,countInfo);
#endif
		}
		else
		{
			AppAddress sid=mServiceManager->getServiceProvider(package->getApp(),mSid,mInfo,package->getHeader().code);
			if(sid.Id!=0 && sid!=mSid)
			{
				post(response,package->getContent(),package->getSize(),sid);
#ifdef TONGJI
				CountInfo countInfo;   
				countInfo.inRequest=1;
				countInfo.inRequestB=package->getSize();
				mServiceManager->updateCountInfo(sid.value,countInfo);
#endif
			}
			else //send error response msg
			{
				reply(package,MsgExpress::ERRCODE_NOSERVICE,MsgExpress::ERRMSG_NOSERVICE,response);
				LOG4_ERROR("No service available:%s\r\n",package->getDebugString().c_str());
				CountInfo countInfo;
				countInfo.inResponse=1;
				mServiceManager->updateCountInfo(mSid,countInfo);
			}
		}
	}
	else if(packagetype==PackageHeader::Publish)
	{
		bool answer=true;
		if(package->getCommand()==0 || package->getSerialNum()==0)//从broker发出的publish不应答
			answer=false;
		if(package->getSrcAddr()!=mSid) //不是本app发出的publish不应答
			answer=false;
		if(answer)
		{
			MsgExpress::CommonResponse resp;
			resp.set_retcode(0);
			resp.set_msg("Publish success");
			reply(package,resp,response);
		}
		/*if(package->getHeader().topic>=MsgExpress::TOPIC_SPECIAL_START && package->getHeader().topic<MsgExpress::TOPIC_SPECIAL_END)
		{
			simplePublish2(package->getCommand(),package->getSerialNum(),package->getSrcAddr(),package,response);
		}
		else*/
		{
			if(package->getHeader().pubmsg)
			{
				publish(package->getCommand(),package->getSerialNum(),package->getSrcAddr(),package,response);
			}
			else
			{
				std::string msgtype=CommandConfig::getInstance().getClazz(package->getHeader().command,(PackageHeader::MsgType)package->getHeader().type,package->getHeader().pubmsg);
				package->setClassType(msgtype);
				bool ret=MessageUtil::parse(package);
				if(ret)
				{
					publish(package->getCommand(),package->getSerialNum(),package->getSrcAddr(),package,response);
				}
				else
				{
					LOG4_ERROR("Error:Publish,parse failed");
				}
			}
		}
	}
	else if(packagetype==PackageHeader::Response)
	{
		unsigned int dst=package->getDstAddr(); 
		AppAddress sid(dst);
		post(response,package->getContent(),package->getSize(),sid);
#ifdef TONGJI
		CountInfo countInfo;
		countInfo.inResponse=1;
		countInfo.inResponseB=package->getSize();
		mServiceManager->updateCountInfo(sid.value,countInfo);
#endif
	}
	else
		LOG4_ERROR("%s",package->getDebugString().c_str());
}

int MsgHandler :: handle( SP_Request * request, SP_Response * response )
{
	MsgDecoder * decoder = (MsgDecoder*)request->getMsgDecoder();

	static unsigned int syncThreadId=0;
	sp_thread_t threadId=sp_thread_self();
	if(syncThreadId==0)
	{
		syncThreadId=threadId;
		LOG4_INFO("Thread %d for sync message handle",syncThreadId);
	}
	SP_SafeQueue* queue=NULL;
	/*if(syncThreadId==threadId)
		queue=decoder->getSyncQueue();
	else
		queue=decoder->getAsyncQueue();*/

	int ret = 0;
	int count=0;
	int index=0;
	for(int i=0;i<2;i++)
	{
		if(i==0)
			queue=decoder->getSyncQueue();
		else
			queue=decoder->getAsyncQueue();
		for( ;queue->getLength()>0; ) 
		{
			if(stoped)
				break;
			void* p=queue->pop();
			Package * package = (Package*)p;
			if(package!=NULL){
				//if(i==0)
				//	LOG4_DEBUG("Prepare process a sequence msg:%s",package->getDebugString().c_str());
				process(request,package,response);
				delete package;
				count++;
			}
			CountInfo countInfo;
			countInfo.queueLength=queue->getLength();
			mServiceManager->updateCountInfo(mSid,countInfo);
		}
	}
	return ret;
}

void MsgHandler :: error( SP_Response * response )
{
	MsgExpress::AppInfo info;
	if(mServiceManager->getAppInfo(mSid,info))
	{
		LOG4_INFO("User error,addr=%s,info:%s", mSid.toString().c_str(), info.ShortDebugString().c_str());
	}
	else
	{
		LOG4_WARN("get app info failed,addr=%s", mSid.toString().c_str());
		LOG4_INFO("User error,addr=%s", mSid.toString().c_str());
	}
}

void MsgHandler :: timeout( SP_Response * response )
{
	MsgExpress::AppInfo info;
	if(mServiceManager->getAppInfo(mSid,info))
	{
		LOG4_INFO("User timeout,addr=%s,info:%s", mSid.toString().c_str(), info.ShortDebugString().c_str());
	}
	else
	{
		LOG4_ERROR("get app info failed,addr=%s", mSid.toString().c_str());
		LOG4_INFO("User timeout,addr=%s", mSid.toString().c_str());
	}
}

int MsgHandler :: start( SP_Request * request, SP_Response * response )
{
	request->setMsgDecoder( new MsgDecoder(mServiceManager->getRecvQueueSize(),true) );
	mSid = response->getFromSid().value;
	LOG4_INFO("User online,addr=%s,ip:%s", mSid.toString().c_str(), request->getClientIP());
	return 0;
}

void MsgHandler :: close( SP_Response * response )
{
	stoped=true;
	MsgExpress::AppInfo info;
	if(mServiceManager->getAppInfo(mSid,info))
	{
		LOG4_INFO("User offline,addr=%s,info:%s", mSid.toString().c_str(), info.ShortDebugString().c_str());

		time_t tm;
		time(&tm);
		MsgExpress::PublishData pub;
		pub.set_topic(MsgExpress::TOPIC_LOGOUT);
		PublishHelperPtr helper=CreatePublishHelper(&pub);
		helper->AddString(MsgExpress::KEY_BROKER,mServiceManager->getBrokerName());
		helper->AddUInt32(MsgExpress::KEY_ADDR,mSid);
		helper->AddDatetime(MsgExpress::KEY_TIME,tm);
		helper->AddString(MsgExpress::KEY_UUID,info.logininfo().uuid());
		helper->AddString(MsgExpress::KEY_NAME,info.logininfo().name());
		publish(0,0,0,&pub,response);
	}
	else
	{
		//LOG4_ERROR("get app info failed,addr=%d",mSid.value);
		LOG4_INFO("User offline,addr=%s",mSid.toString().c_str());
	}

	mServiceManager->unregApp(mSid);
	if(mServiceList.size()>0)
		mServiceManager->unregService(mServiceList,mSid);
	mServiceManager->unSubscribeAll(mSid);
}


void MsgCompletionHandler :: completionMessage( SP_Message * msg )
{
#if 0
	printf( "message completed { completion key : %d }\n", msg->getCompletionKey() );

	printf( "\tsuccess {" );
	for( int i = 0; i < msg->getSuccess()->getCount(); i++ ) {
		printf( " %d", msg->getSuccess()->get( i ).mKey );
	}
	printf( "}\n" );

	printf( "\tfailure {" );
	for( int i = 0; i < msg->getFailure()->getCount(); i++ ) {
		printf( " %d", msg->getFailure()->get( i ).mKey );
	}
	printf( "}\n" );
#endif

	delete msg;
}
