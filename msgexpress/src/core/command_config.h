#pragma once

#include "../inc/package.h"
#include "xmlconfig.h"
#include <string>
#include <map>
#include <vector>
#include <mutex>

using namespace std;

class MSGEXPRESS_API FunctionConfig
{
private:
	struct Data{
		map<int, string> mapCommand2Class;
		map<string, int> mapClass2Command;
		map<string, int> mapClass2Service;
		map<int, std::shared_ptr< MsgExpress::RegisterService>> mapServiceInfo;
		void Clear()
		{
			mapCommand2Class.clear();
			mapClass2Command.clear();
			mapClass2Service.clear();
			mapServiceInfo.clear();
		}
		void CopyTo(Data* dest)
		{
			dest->mapCommand2Class = mapCommand2Class;
			dest->mapClass2Command = mapClass2Command;
			dest->mapClass2Service = mapClass2Service;
			dest->mapServiceInfo = mapServiceInfo;
		}
	};
	Data* data1_;
	Data* data2_;
	Data* curData;
	std::mutex mtx;
private:
	int Hashcode(const char* str);
	void CopyData(Data* src, Data* dest);
	void Register(Data* data,int serviceid, const char* classname);
public:
	FunctionConfig();
	~FunctionConfig();

	int GetCommand(const char* classname);
	string GetClass(int command);
	int GetServiceId(const char* classname);
	int GetServiceId(int command);
	void Register(int serviceid, const vector<string>& vecClassname);
	void Register(std::shared_ptr< MsgExpress::RegisterService> registerservice);
	map<int, std::shared_ptr< MsgExpress::RegisterService>> GetAllServiceInfo();
};

struct MSGEXPRESS_API CommandConfig
{
	static FunctionConfig& getInstance(){
		static FunctionConfig config;
		return config;
	}
};

