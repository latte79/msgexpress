#include "connection.h"
#include "../inc/logger.h"
#include "command_config.h"
#include "ioutils.h"
#include "messageutil.h"

void readcb(bufferevent *bev, void *arg)
{
	Connection* conn = (Connection*)arg;
	conn->read();
}

void writecb(bufferevent *bev, void *arg)
{
	Connection* conn = (Connection*)arg;
}

void eventcb(struct bufferevent *bev, short what, void *arg)
{
	Connection* conn = (Connection*)arg;
	conn->error(what);
}

void Connection::read()
{
	if (stoped)
		return;
	struct evbuffer *input = bufferevent_get_input(bev);

	if (protocol() == P_NONE)
	{
		char tmp[5];
		evbuffer_copyout(input, tmp, 4);
		tmp[4] = 0;
		if (strcmp(tmp, "GET ") == 0)
		{
			size_t total = evbuffer_get_length(input);
			string data;
			data.resize(total);
			evbuffer_copyout(input, (char*)data.data(), total);
			if (data.find("WebSocket-Version") != std::string::npos && data.find("WebSocket-Key") != std::string::npos)
			{
				LOG4_INFO("Protocol is websocket");
				string retStr;
				if (wsHandler->process(input, retStr) != 0)
				{
					LOG4_ERROR("Websocket error.");
					error(0);
					return;
				}
				if (!wsHandler->isConnected())
				{
					LOG4_ERROR("Websocket connect failed.");
					error(0);
				}
				else
				{
					LOG4_INFO("Websocket connect success");
					set_protocol ( P_WEBSOCKET);
				}
				return;
			}
			else if (data.find("HTTP") != std::string::npos && data.find("Connection") != std::string::npos)
			{
				LOG4_INFO("Protocol is http");
				set_protocol ( P_HTTP);
				
			}
			
		}
		else if (strcmp(tmp, "POST") == 0)
		{
			size_t total = evbuffer_get_length(input);
			string data;
			data.resize(total);
			evbuffer_copyout(input, (char*)data.data(), total);
			if (data.find("HTTP") != std::string::npos && data.find("Connection") != std::string::npos)
			{
				LOG4_INFO("Protocol is http");
				set_protocol(P_HTTP);

			}
		}
		else if (tmp[0] == PackageFlag && tmp[1] == PackageFlag)
			set_protocol( P_SOCKET);
	}

	if (protocol() == P_WEBSOCKET)
	{
		string retStr;
		if (wsHandler->process(input, retStr) != 0)
			return;
		evbuffer_add(evbufWebsocket, retStr.data(), retStr.size());
		Package *package = decoder->decode(evbufWebsocket);
		while (package)
		{
			LOG4_DEBUG("Connection onRead,decode a msg = %s", package->DebugString().c_str());
			if (!package->IsPublish())//publish可能是转发的包，所以不设置原地址
				package->SetSrcAddr(mAddr);
			if (onPackage)
				onPackage(PackagePtr(package));
			package = decoder->decode(evbufWebsocket);
		}

	}
	else if (protocol() == P_HTTP)
	{
		size_t total = evbuffer_get_length(input);
		string data;
		data.resize(total);
		evbuffer_copyout(input, (char*)data.data(), total);
		httpParser->append(data.data(), data.size());
		if (httpParser->isCompleted())
		{
			evbuffer_drain(input, total);
			HttpRequest* request = httpParser->getRequest();
			mapRequest.insert(std::make_pair(serialNum, request));
			const char* uri = request->getURI();
			uri++;
			string params = request->getJsonParam();
			LOGGER_INFO("http request,uri:" << uri << ",param:" << params);
			PackageHeader header;
			int cmd = CommandConfig::getInstance().GetCommand(uri);
			if (cmd != 0)
			{
				header.set_srcaddr(this->mAddr);
				header.set_command(cmd);
				header.set_protocol(PackageHeader::CodeType::JSON);
				header.set_bodysize(params.size());
				header.set_type(PackageHeader::MsgType::Request);
				header.set_serialnum(serialNum);
				int total = header.bodysize() + header.offset();
				string packdata;
				packdata.resize(total);
				header.write((char*)packdata.data());
				memcpy((char*)packdata.data() + header.offset(), params.data(), params.size());
				Package *package = new Package(header, uri, (const char*)packdata.data(), header.bodysize() + header.offset(), MessagePtr());
				if (onPackage)
					onPackage(PackagePtr(package));
			}
			else
			{
				string errmsg = "Cant recognize command:" + string(uri);
				header.set_srcaddr(this->mAddr);
				header.set_command(0);
				header.set_protocol(PackageHeader::CodeType::JSON);
				header.set_bodysize(errmsg.size());
				header.set_type(PackageHeader::MsgType::Response);
				header.set_serialnum(serialNum);
				string sendMsg;
				MessageUtil::serializePackageToString(header, errmsg.data(), errmsg.size(), false, 0, &sendMsg);
				this->sendMsg(sendMsg);
				LOGGER_ERROR("Cant recognize command:" << uri);
			}
			serialNum++;
		}
	}
	else if (protocol() == P_SOCKET)
	{
		Package *package = decoder->decode(input);
		while (package)
		{
			LOG4_DEBUG("Connection onRead,decode a msg = %s", package->DebugString().c_str());
			if (!package->IsPublish())//publish可能是转发的包，所以不设置原地址
				package->SetSrcAddr(mAddr);
			if (onPackage)
				onPackage(PackagePtr(package));
			package = decoder->decode(input);
		}
	}
	else if (protocol() == P_HTTP)
	{
		LOG4_ERROR("Http protocol.to be processed");
	}
	else
	{
		LOG4_ERROR("Unknow protocol.");
		error(1);
	}
	
}



void Connection::error(short error)
{
	LOG4_INFO("Connection onError , addr = %s", mAddr.toString().c_str());
	if (error & BEV_EVENT_TIMEOUT)
	{
		cout << "Connection addr = " << mAddr << " EVBUFFER_TIMEOUT " << endl;
	}
	if (error & BEV_EVENT_EOF)
	{
		cout << "Connection addr = " << mAddr << " EVBUFFER_EOF " << endl;
	}
	if (error & BEV_EVENT_ERROR)
	{
		cout << "Connection addr = " << mAddr << " EVBUFFER_ERROR " << endl;
	}
	close();
	if (onError)
		onError(error);
}

void Connection::close()
{
	std::unique_lock<std::mutex> lock(mutexStop);
	if (stoped)
		return;
	stoped = true;
	lock.unlock();
	LOG4_INFO("Begin close, addr = %s", mAddr.toString().c_str());
	//关闭evbuffer和回调
	bufferevent_disable(bev, EV_READ | EV_WRITE);
	bufferevent_setcb(bev, NULL, NULL, NULL, this);
	
	intptr_t tmp = sockInfo.sock;
	sockInfo.sock = INVALID_SOCKET;
	if (tmp == INVALID_SOCKET)
		return;
	LOGGER_INFO("close socket  " << tmp << ",addr=" << mAddr.toString().c_str());
#ifdef _WIN32
	closesocket(tmp);
#else
	::close((int)tmp);
#endif
	
}

unsigned int send2(intptr_t sock, char * data, unsigned int size)
{
	iovec vec;
	vec.iov_base = data;
	vec.iov_len = size;
	return sp_writev(sock, &vec, 1);

}

void Connection::sendMsg(const string& msg)
{
	if (stoped)
		return;

	bool needSend = false;
	string sendData;
	if (protocol() == P_WEBSOCKET)
	{
		if (wsHandler)
		{
			//string outStr;
			wsHandler->encodeMsg(msg, sendData);
			/*std::unique_lock<std::mutex> lock(mutexSend);
			evbuffer_add(evbufSend, outStr.data(), outStr.size());
			needSend = true;*/
		}
	}
	else if (protocol() == P_HTTP)
	{
		PackageHeader header;
		header.read(msg.data());
		
		map<unsigned int,HttpRequest*>::const_iterator it=mapRequest.find(header.serialnum());
		if (it == mapRequest.end())
		{
			LOGGER_ERROR("Cant find HttpRequest for serialnum:" << header.serialnum());
		}
		else
		{
			HttpRequest* request = it->second;
			mapRequest.erase(it);
			HttpResponse response;
			response.setVersion(request->getVersion());
			response.appendContent((char*)msg.substr(header.offset()).c_str());

			char buffer[128];
			// check keep alive header
			if (request->isKeepAlive()) {
				if (response.getHeaderValue(HttpMessage::HEADER_CONNECTION).size()==0) {
					response.addHeader(HttpMessage::HEADER_CONNECTION, "Keep-Alive");
				}
			}

			// check Content-Length header
			response.removeHeader(HttpMessage::HEADER_CONTENT_LENGTH);
			if (response.getContentLength() > 0) {
				snprintf(buffer, sizeof(buffer), "%d", response.getContentLength());
				response.addHeader(HttpMessage::HEADER_CONTENT_LENGTH, buffer);
			}

			// check date header
			response.removeHeader(HttpMessage::HEADER_DATE);
			time_t tTime = time(NULL);
			struct tm tmTime;
			gmtime_r(&tTime, &tmTime);
			strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S", &tmTime);
			response.addHeader(HttpMessage::HEADER_DATE, buffer);

			// check Content-Type header
			if (response.getHeaderValue(HttpMessage::HEADER_CONTENT_TYPE).size()==0) {
				response.addHeader(HttpMessage::HEADER_CONTENT_TYPE,"text/html; charset=UTF-8");
			}

			response.removeHeader(HttpMessage::HEADER_CONTENT_LANGUAGE);
			response.addHeader(HttpMessage::HEADER_CONTENT_LANGUAGE, "zh-CN");
			// check Server header
			response.removeHeader(HttpMessage::HEADER_SERVER);
			response.addHeader(HttpMessage::HEADER_SERVER, "http/msgexpress");

			sendData = response.toString();
			sendData += "\r\n";
			sendData += msg.substr(header.offset());
			sendData += "\r\n";
			
			httpParser->clear();
		}
	}
	else
		sendData = msg;
	{
		std::unique_lock<std::mutex> lock(mutexSend);
		size_t size = evbuffer_get_length(evbufSend);

		if (size == 0)
		{
			int sendSize = send2(sockInfo.sock, (char*)sendData.data(), sendData.size());
			if (sendSize > 0 && sendSize < (int)sendData.size())
			{
				LOG4_INFO("Connection::sendMsg,want send:%d,real send=%d", sendData.size(), sendSize);
				evbuffer_add(evbufSend, sendData.data() + sendSize, sendData.size() - sendSize);
				needSend = true;
			}
			else if (sendSize < 0)
			{
				PackageHeader header;
				header.read(sendData.data());
				LOG4_ERROR("Write socket error,ignore data=%s,addr=%s", header.DebugString().c_str(), mAddr.toString().c_str());
				error(0);
			}
			else
			{
				//needSend = true; //避免客户端不收数据把服务端拖死
			}
		}
		else
		{
			evbuffer_add(evbufSend, sendData.data(), sendData.size());
			needSend = true;
		}
	}
	if (onSend)
		onSend(needSend,nullptr);
}

void Connection::executeSend(void* arg)
{
	bool needSend = false;
	std::unique_lock<std::mutex> lock(mutexSend);
	size_t size = evbuffer_get_length(evbufSend);
	if (!stoped && size > 0)
	{
		unsigned char* data = NULL;
		if (size > 100000)
		{
			size = 100000;
			needSend = true;
		}
		data = evbuffer_pullup(evbufSend, size);
		int sendSize = 0;
		if (data)
			sendSize = send(sockInfo.sock, (char*)data, size, 0);
		if (sendSize > 0)
		{
			evbuffer_drain(evbufSend, sendSize);
			if (sendSize < (int)size)
			{
				LOG4_INFO("Connection::executeSend,want send:%d,real send=%d,addr=%s", size, sendSize, mAddr.toString().c_str());
				needSend = true;
			}
		}
		else if (sendSize < 0)
		{
			//evbuffer_drain(evbufSend, size);
			LOG4_ERROR("Write socket error,drain data=%d,addr=%s", size, mAddr.toString().c_str());
			error(0);
		}
		else
		{
			//needSend = true; //避免客户端不收数据把服务端拖死
		}
	}
	if (onSend)
		onSend(needSend,arg);
}
Connection::Connection(event_base* base, const SockInfo& sockinfo, int buffersize, int timeout)
{
	stoped = false;
	memcpy(&sockInfo, &sockinfo, sizeof(sockInfo));
	wsHandler = new Websocket_Handler(sockInfo.sock);
	httpParser = new HttpMsgParser();
	serialNum = 1;
	evbufWebsocket = evbuffer_new();
	set_protocol( P_NONE);
	mAddr = 0;
	LOG4_INFO("New Connection addr = %s,ip=%s,sock=%d", mAddr.toString().c_str(), sockInfo.ip, sockInfo.sock);
	evbufSend = evbuffer_new();
	decoder = new MsgDecoder(100);
	bev = nullptr;

	evutil_make_socket_nonblocking(sockInfo.sock);
	IOUtils::setBuffer(sockInfo.sock, buffersize, buffersize);
	// 新建一个bufferevent，设定BEV_OPT_CLOSE_ON_FREE， 
	// 保证bufferevent被free的时候fd也会被关闭 
	//调用了evthread_use_windows_threads，这里BEV_OPT_THREADSAFE才有效
	bev = bufferevent_socket_new(base, sockInfo.sock, BEV_OPT_CLOSE_ON_FREE/* | BEV_OPT_THREADSAFE*/);
	if (!bev) {
		LOG4_ERROR("Error constructing bufferevent!");
		event_base_loopbreak(base);
		return;
	}

	bufferevent_setwatermark(bev, EV_READ, 30, 10000000);
	bufferevent_setwatermark(bev, EV_WRITE, 30, 10000000);
	// 设定写buffer的event和其它event 
	bufferevent_setcb(bev,readcb, writecb, eventcb, this);
	// 开启向fd中写的event 
	bufferevent_enable(bev, EV_WRITE);
	bufferevent_enable(bev, EV_READ);

	struct timeval writeTimeout;
	writeTimeout.tv_sec = timeout;//超时时间（秒），
	writeTimeout.tv_usec = 0;

	struct timeval readTimeout;
	readTimeout.tv_sec = timeout;//超时时间（秒），
	readTimeout.tv_usec = 0;
	bufferevent_set_timeouts(bev, &readTimeout, nullptr);////超时时间（秒），
}

Connection::~Connection()
{
	close();
	delete decoder;
	delete wsHandler;
	delete httpParser;
	evbuffer_free(evbufWebsocket);
	evbuffer_free(evbufSend);
	bufferevent_free(bev);
	bev = nullptr;
	LOG4_INFO("Connection::~Connection ,addr=%s", mAddr.toString().c_str());
}