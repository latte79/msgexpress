// 异步消息超时回调接口
package com.databus;

public interface TimeOutCallBack {

	public void OnMessageTimeOut(int serNum);
	
}
