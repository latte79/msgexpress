// 发送异步消息应答回调接口类
package com.databus;

import com.google.protobuf.GeneratedMessage;
import com.packet.PackageData;


public interface PostMsgCallBack {
	
	public void OnResponse(MsgParams params);

	public class MsgParams
	{
		public int result;                  // 0表示成功，非0表示失败
		public String errMsg;               // 错误描述
		public Object req = null; // 请求消息
		public PackageData rspData = null;  // 应答消息
		public Object arg = null;           // 附加参数
	}
}
