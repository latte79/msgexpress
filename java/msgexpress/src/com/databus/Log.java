// 鏃ュ織鎵撳嵃绫诲皝瑁咃紝鍩轰簬log4j
package com.databus;

public class Log {
	public interface LogInterface {
		public void trace(Object obj);
		public void debug(Object obj);
	    public void info(Object obj);
	    public void warn(Object obj);
	    public void error(Object obj);
	    public void fatal(Object obj);
	    
	    public void error(Exception e);
	    public void fatal(Exception e);
	}
	private static LogInterface log_;
	public static void SetLog(LogInterface log)
	{
		log_=log;
	}
	public static void trace(Object obj)
	{
		if(log_!=null)
			log_.trace(obj);
		//else
		//	System.out.println("trace:"+obj.toString());
	}
	public static void debug(Object obj)
	{
		if(log_!=null){
			//log_.debug(obj);
		}
		//else
		//	System.out.println("debug:"+obj.toString());
	}
	public static void info(Object obj)
	{
		if(log_!=null)
			log_.info(obj);
		else
			System.out.println("info:"+obj.toString());
	}
	public static void warn(Object obj)
	{
		if(log_!=null)
			log_.warn(obj);
		else
			System.out.println("warn:"+obj.toString());
	}
	public static void error(Object obj)
	{
		if(log_!=null)
			log_.error(obj);
		else
			System.out.println("error:"+obj.toString());
	}
	public static void error(Exception e)
	{
		if(log_!=null)
			log_.error(e);
		else
			System.out.println("error:"+e.toString());
	}
	public static void fatal(Object obj)
	{
		if(log_!=null)
			log_.fatal(obj);
		else
			System.out.println("fatal:"+obj.toString());
	}
}


