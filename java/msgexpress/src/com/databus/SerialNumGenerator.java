// 消息序列号生成类
package com.databus;

public class SerialNumGenerator {
	public static final int MaxSyncSerialNum = 0xFFFF;
	public static final int MinAsynSerailNum = 0x10000;
	public static final int MaxAsynSerailNum = Integer.MAX_VALUE;
	
	// 获取同步流水号
	public static synchronized int GetSyncNum()
	{
		mSyncNum++;
		if(mSyncNum >= MaxSyncSerialNum)
		{
			mSyncNum = 1;
		}
		return mSyncNum;
	}
	
	// 获取异步流水号
	public static synchronized int GetAsynNum()
	{
		mAsynNum++;
		if(MinAsynSerailNum > mAsynNum || mAsynNum >= MaxAsynSerailNum)
		{
			mAsynNum = MinAsynSerailNum;
		}
		return mAsynNum;
	}

	private static int mSyncNum = 0;
	private static int mAsynNum = 0;
}
